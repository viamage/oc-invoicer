<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 9/29/17
 * Time: 8:39 AM
 */

namespace Viamage\Invoicer\ValueObjects;

use Viamage\Invoicer\Models\Customer;
use Viamage\Invoicer\Models\Settings;

/**
 * Class Company
 * @package Viamage\Invoicer\ValueObjects
 */
class Company
{
    /**
     * @var string
     */
    public $company;
    /**
     * @var string
     */
    public $address1;
    /**
     * @var string
     */
    public $address2;
    /**
     * @var string
     */
    public $zip;
    /**
     * @var string
     */
    public $city;
    /**
     * @var string
     */
    public $country;
    /**
     * @var string
     */
    public $vatNumber;
    /**
     * @var string
     */
    public $regNumber;
    /**
     * @var string
     */
    public $email;
    /**
     * @var string
     */
    public $phone;

    /**
     * Loads from settings
     */
    public function fromSettings()
    {
        $settings = Settings::instance();
        $this->company = $settings->get('company');
        $this->address1 = $settings->get('address_1');
        $this->address2 = $settings->get('address_2');
        $this->zip = $settings->get('zip');
        $this->city = $settings->get('city');
        $this->country = $settings->get('country');
        $this->vatNumber = $settings->get('vat_number');
        $this->regNumber = $settings->get('reg_number');
        $this->email = $settings->get('email');
        $this->phone = $settings->get('phone');
    }

    /**
     * @param Customer $model
     */
    public function fromModel(Customer $model)
    {
        $this->company = $model->company;
        $this->address1 = $model->address_1;
        $this->address2 = $model->address_2;
        $this->zip = $model->zip;
        $this->city = $model->city;
        $this->country = $model->country;
        $this->vatNumber = $model->vat_number;
        $this->regNumber = $model->reg_number;
        $this->email = $model->email;
        $this->phone = $model->phone;
    }
}