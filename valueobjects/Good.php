<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 9/28/17
 * Time: 4:58 PM
 */

namespace Viamage\Invoicer\ValueObjects;

use Keios\MoneyRight\Money;
use October\Rain\Exception\ApplicationException;
use Viamage\Invoicer\Models\Invoice;
use Validator;

/**
 * Class Good
 * @package Viamage\Invoicer\ValueObjects
 */
class Good
{
    /**
     * @var string
     */
    public $title;
    /**
     * @var Money
     */
    public $unitNetPrice;
    /**
     * @var Money
     */
    public $unitEurNetPrice;

    /**
     * @var int
     */
    public $units;
    /**
     * @var int
     */
    public $taxRate;

    /**
     * @var Money
     */
    public $unitTaxAmount;

    /**
     * @var Money
     */
    public $unitEurTaxAmount;

    /**
     * @var Money
     */
    public $unitGrossPrice;
    /**
     * @var Money
     */
    public $unitEurGrossPrice;
    /**
     * @var Money
     */
    public $totalNetPrice;

    /**
     * @var Money
     */
    public $totalEurNetPrice;

    /**
     * @var Money
     */
    public $totalTaxAmount;

    /**
     * @var Money
     */
    public $totalEurTaxAmount;

    /**
     * @var Money
     */
    public $totalGrossPrice;

    /**
     * @var Money
     */
    public $totalEurGrossPrice;

    /**
     * @param array  $good
     * @param float  $ratio
     * @param string $currencyIso
     * @throws \ValidationException
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function fromModelEntry(array $good, float $ratio, string $currencyIso)
    {
        $rules = [
            'title'        => 'required',
            'unit_price'   => 'required',
            'units_amount' => 'required',
            'vat_rate'     => 'required',
        ];
        $v = Validator::make($good, $rules);
        if ($v->fails()) {
            throw new \ValidationException($v);
        }

        $this->title = $good['title'];
        $this->units = $good['units_amount'];
        $this->taxRate = $good['vat_rate'];

        $this->unitNetPrice = Money::$currencyIso($good['unit_price']);
        $this->unitTaxAmount = $this->unitNetPrice->multiply($this->taxRate / 100);
        $this->totalTaxAmount = $this->unitTaxAmount->multiply($this->units);
        $this->unitGrossPrice = $this->unitNetPrice->add($this->unitTaxAmount);
        $this->totalNetPrice = $this->unitNetPrice->multiply($this->units);
        $this->totalGrossPrice = $this->unitGrossPrice->multiply($this->units);

        if ($currencyIso === 'EUR') {
            $this->unitEurNetPrice = $this->unitNetPrice;
            $this->unitEurTaxAmount = $this->unitTaxAmount;
            $this->totalEurTaxAmount = $this->totalTaxAmount;
            $this->unitEurGrossPrice = $this->unitGrossPrice;
            $this->totalEurNetPrice = $this->totalNetPrice;
            $this->totalEurGrossPrice = $this->totalGrossPrice;
        } else {
            $this->unitEurNetPrice = Money::EUR($this->unitNetPrice->divide($ratio)->getAmountString());
            $this->unitEurTaxAmount = Money::EUR($this->unitTaxAmount->divide($ratio)->getAmountString());
            $this->totalEurTaxAmount = Money::EUR($this->totalTaxAmount->divide($ratio)->getAmountString());
            $this->unitEurGrossPrice = Money::EUR($this->unitGrossPrice->divide($ratio)->getAmountString());
            $this->totalEurNetPrice = Money::EUR($this->totalNetPrice->divide($ratio)->getAmountString());
            $this->totalEurGrossPrice = Money::EUR($this->totalGrossPrice->divide($ratio)->getAmountString());
        }

    }
}