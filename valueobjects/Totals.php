<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 9/29/17
 * Time: 8:57 AM
 */

namespace Viamage\Invoicer\ValueObjects;

use Keios\MoneyRight\Money;

/**
 * Class Totals
 * @package Viamage\Invoicer\ValueObjects
 */
class Totals
{
    /**
     * @var Money
     */
    public $totalNet;
    /**
     * @var Money
     */
    public $totalTax;
    /**
     * @var Money
     */
    public $totalEurNet;
    /**
     * @var Money
     */
    public $totalEurTax;
    /**
     * @var Money
     */
    public $totalGross;
    /**
     * @var Money
     */
    public $totalEurGross;

    /**
     * Totals constructor.
     * @param string $currency
     */
    public function __construct(string $currency = 'EUR')
    {
        $this->totalNet = Money::$currency(0);
        $this->totalTax = Money::$currency(0);
        $this->totalEurNet = Money::EUR(0);
        $this->totalEurTax = Money::EUR(0);
    }
}