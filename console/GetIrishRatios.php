<?php
/**
 * Copyright (c) 2018 Viamage Limited
 * All Rights Reserved
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Viamage Limited and its suppliers, if any.
 *  The intellectual and technical concepts contained herein
 *  are proprietary to Viamage Limited and its suppliers and are
 *  protected by trade secret or copyright law, if not specified otherwise.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Viamage Limited.
 *
 */

/**
 * Created by PhpStorm.
 * User: jin
 * Date: 10/2/17
 * Time: 4:47 PM
 */

namespace Viamage\Invoicer\Console;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Viamage\Invoicer\Contracts\CurrencyConverter;
use Viamage\Invoicer\Models\CurrencyRatio;

class GetIrishRatios extends Command
{
    /**
     * The console command name.
     */
    protected $name = 'invoicer:get-irish-ratios';

    /**
     * The console command description.
     */
    protected $description = 'Downloads Irish Ratios';

    /**
     * Execute the console command.
     *
     * @throws \Exception
     * @throws \InvalidArgumentException
     * @throws \ApplicationException
     */
    public function handle()
    {
        /** @var CurrencyConverter $converter */
        $converter = \App::make(CurrencyConverter::class);
        $date = Carbon::now()->toDateString();
        $ratios = $converter->readFor($date);
        $ratioModel = new CurrencyRatio();
        $ratioModel->date = $date;
        $ratioModel->ratios = json_encode($ratios);
        $ratioModel->save();
        $this->info('Successfully downloaded ratios');
    }
    /**
     * Get the console command arguments.
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     */
    protected function getOptions()
    {
        return [];
    }
}