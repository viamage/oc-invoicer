<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 10/2/17
 * Time: 4:47 PM
 */

namespace Viamage\Invoicer\Console;

use Illuminate\Console\Command;
use Keios\Apparatus\Classes\JobManager;
use Symfony\Component\Console\Input\InputArgument;
use Viamage\Invoicer\Models\Invoice;

class GeneratePdf extends Command
{
    /**
     * The console command name.
     */
    protected $name = 'invoicer:generate-pdf';

    /**
     * The console command description.
     */
    protected $description = 'Triggers Generate PDF Job';

    /**
     * Execute the console command.
     *
     * @throws \Exception
     * @throws \InvalidArgumentException
     * @throws \ApplicationException
     */
    public function handle()
    {
        $invoiceId = $this->argument('invoiceId');
        /** @var Invoice $invoice */
        $invoice = Invoice::where('id', $invoiceId)->first();
        /** @var JobManager $jobManager */
        $jobManager = \App::make(JobManager::class);
        if(!$invoice){
            $this->error('Invoice not found');
            return null;
        }
        $metadata = [
            'ID'       => $invoice->id,
            'Number'   => $invoice->getInvoiceNumber(),
            'Link'     => url($invoice->getInvoiceLink()),
            'Internal' => $invoice->getInvoiceInternalLink(),
        ];
        $properties = [
            'metadata' => $metadata,
            'count'    => 1,
        ];
        $job = new \Viamage\Invoicer\Jobs\GeneratePdf($invoice->id);
        $jobId = $jobManager->dispatch($job, 'Console-triggered PDF Generation for '.$invoice->getInvoiceNumber(), $properties);
        $this->info('Generation job dispatched under ID ' . $jobId);
    }
    /**
     * Get the console command arguments.
     */
    protected function getArguments()
    {
        return [
            [
                'invoiceId',
                InputArgument::REQUIRED,
                'Invoice ID',
            ]

        ];
    }

    /**
     * Get the console command options.
     */
    protected function getOptions()
    {
        return [];
    }
}