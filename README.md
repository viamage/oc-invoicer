# Viamage Invoicer

This plugin allows to create incoming and outgoing invoices, generate PDF from them using google-chrome headless mode and generate statistics and accounts CSVs.

### Installation

Clone the repository into `october_root/plugins/viamage/invoicer`

Issue `composer update` and `php artisan october:up`

Generating PDFs is usually pretty fast, so you can leave queue engine as `sync`.

### Configuration

1. Go to Backend -> Settings -> Viamage Invoicer Settings
2. Select country. // currently only Ireland is supported. 
3. Setup `Due soon` field, recommended value is `3`.
4. Setup Google Chrome path
5. Fill `Invoice Issuer` tab with your company data.
6. Go to Backend -> Invoicing -> Customers
7. Create your customers.

### Usage

Simply create invoices. Statistics will be done automatically. 