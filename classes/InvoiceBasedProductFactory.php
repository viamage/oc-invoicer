<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/27/17
 * Time: 2:21 PM
 */

namespace Viamage\Invoicer\Classes;

use Keios\MoneyRight\Money;
use Keios\PaymentGateway\ValueObjects\Item;
use Viamage\Invoicer\Models\Invoice;

/**
 * Class InvoiceBasedProductFactory
 * @package Viamage\Invoicer\Classes
 */
class InvoiceBasedProductFactory
{
    /**
     * @param $invoiceItem
     * @param $currency
     * @return InvoiceItem
     * @internal param Invoice $invoice
     */
    public function convertInvoiceItemToItem($invoiceItem, $currency)
    {
        $this->validateItemArray($invoiceItem);
        $netPriceFloat = $invoiceItem['unit_price'];
        $netPrice = Money::$currency($netPriceFloat);

        return new InvoiceItem(
            $invoiceItem['title'],
            (int)$invoiceItem['units_amount'],
            $netPrice,
            (int)$invoiceItem['vat_rate'],
            []
        );
    }

    private function validateItemArray($invoiceItem)
    {
        $rules = [
            'title'        => 'required',
            'unit_price'   => 'required',
            'units_amount' => 'required',
            'vat_rate'     => 'required',
        ];
        $v = \Validator::make($invoiceItem, $rules);
        if ($v->fails()) {
            throw new \ValidationException($v);
        }
    }
}