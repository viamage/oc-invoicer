<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/27/17
 * Time: 2:18 PM
 */

namespace Viamage\Invoicer\Classes;


use Illuminate\Session\SessionManager;
use Keios\PaymentGateway\ValueObjects\Cart;
use Viamage\Invoicer\Contracts\CartStoreInterface;

class SessionCartStore implements CartStoreInterface
{

    /**
     * @constant
     */
    const SESSION_KEY = 'Market.Cart';

    /**
     * @var \Illuminate\Session\SessionManager
     */
    protected $sessionStore;

    /**
     * @param \Illuminate\Session\SessionManager $sessionStore
     */
    public function __construct(SessionManager $sessionStore)
    {
        $this->sessionStore = $sessionStore;
    }

    /**
     * @param Cart $cart
     * @return mixed|void
     */
    public function save(Cart $cart)
    {
        $this->sessionStore->put(self::SESSION_KEY, $cart);
    }

    /**
     * @return \Keios\PaymentGateway\ValueObjects\Cart
     */
    public function load()
    {
        $cart = $this->sessionStore->get(self::SESSION_KEY);

        if (!$cart) {
            $cart = new Cart();
        }

        return $cart;
    }

    /**
     * @return null
     */
    public function delete()
    {
        $this->sessionStore->forget(self::SESSION_KEY);
    }
}