<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/27/17
 * Time: 2:18 PM
 */

namespace Viamage\Invoicer\Classes;

use Keios\PaymentGateway\ValueObjects\Item;

class InvoiceItem extends Item
{
    const TYPE = 'InvoiceItem';
}