<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 9/30/17
 * Time: 7:09 PM
 */

namespace Viamage\Invoicer\Classes;

use ApplicationException;
use Carbon\Carbon;
use Cms\Classes\Theme;
use Illuminate\Mail\Message;
use System\Models\File;
use Viamage\Invoicer\Models\Customer;
use Viamage\Invoicer\Models\Invoice;
use Mail;
use URL;
use Viamage\Quoter\Models\CustomerPermission;

/**
 * Class Mailer
 * @package Viamage\Invoicer\Classes
 */
class Mailer
{
    /**
     * @var array
     */
    private $componentPageCache;

    /**
     * @param Invoice $model
     * @throws ApplicationException
     * @throws \Exception
     */
    public function sendSoonReminder(Invoice $model)
    {
        $linkGenerator = \App::make(InvoiceLinkGenerator::class);
        $url = URL::to($linkGenerator->getInvoiceLink($model));
        $vars['name'] = $model->getCompany()->company;
        $vars['invoice_number'] = $model->getInvoiceNumber();
        $vars['due_date'] = $model->getDueDate();
        $vars['invoice_link'] = $url;
        $email = $model->getCompany()->email;
        if (!$email) {
            throw new ApplicationException('Customer does not have e-mail in its profile!');
        }
        $this->sendEmail($vars, 'viamage.invoicer::mail.due-reminder', $email);

        $count = $model->reminders_count + 1;
        $model->reminders_count = $count;
        $model->last_reminder = Carbon::now();
        $model->save();
    }

    /**
     * @param Invoice $model
     * @throws ApplicationException
     * @throws \Exception
     */
    public function sendPassedReminder(Invoice $model)
    {
        $linkGenerator = \App::make(InvoiceLinkGenerator::class);
        $url = URL::to($linkGenerator->getInvoiceLink($model));
        $vars['name'] = $model->getCompany()->company;
        $vars['invoice_number'] = $model->getInvoiceNumber();
        $vars['due_date'] = $model->getDueDate();
        $vars['invoice_link'] = $url;
        $email = $model->getCompany()->email;
        if (!$email) {
            throw new ApplicationException('Customer does not have e-mail in its profile!');
        }
        $this->sendEmail($vars, 'viamage.invoicer::mail.due-time', $email);

        $count = $model->reminders_count + 1;
        $model->reminders_count = $count;
        $model->last_reminder = Carbon::now();
        $model->save();
    }

    /**
     * @param Invoice $model
     * @throws ApplicationException
     * @throws \Exception
     */
    public function sendInvoice(Invoice $model)
    {
        $linkGenerator = \App::make(InvoiceLinkGenerator::class);
        $url = URL::to($linkGenerator->getInvoiceLink($model));
        $customer = $model->getCustomer();
        $extension = $customer->extension;
        $billables = $customer->billables;
        $user = $model->user;
        $vars['name'] = $model->getCompany()->company;
        $vars['invoice_amount'] = $model->getTotalGross()->getAmountString().' '.$model->getTotalGross()->getCurrency(
            )->getIsoCode();
        $vars['customer_report_link'] = null;
        if (\count($billables) > 0) {
            $vars['customer_report_link'] = $this->getReportLink($extension);
        }
        $vars['invoice_number'] = $model->getInvoiceNumber();
        $vars['due_date'] = $model->getDueDate();
        $vars['payment_method'] = $model->getPaymentMethod();
        $vars['issuer'] = $user->first_name . ' '. $user->last_name;
        $vars['invoice_link'] = $url;
        $email = $model->getCompany()->email;
        $pdfPath = $this->preparePdf($model);

        if (!$email) {
            throw new ApplicationException('Customer does not have e-mail in its profile!');
        }

        $this->sendEmail($vars, 'viamage.invoicer::mail.new-invoice', $email, $pdfPath);

        $model->is_sent = true;
        $model->save();
    }


    private function sendEmail($vars, $template, $email, $attachmentPath = null){
        Mail::send(
        /**
         * @param Message $message
         */
            $template,
            $vars,
            function (Message $message) use ($email, $vars, $attachmentPath) {
                $message->to($email, $vars['name']);
                if ($attachmentPath) {
                    $message->attach($attachmentPath);
                }
            }
        );
    }


    private function getReportLink(CustomerPermission $model)
    {
        if (!$model) {
            return null;
        }

        return \Config::get('app.url').'/report/'.$model->hash_id.'?code='.str_replace('==', '', $model->security_code);
    }

    private function preparePdf(Invoice $invoice)
    {
        /** @var File $pdf */
        $pdf = $invoice->pdf;
        $path = storage_path('/app/'.$pdf->getDiskPath());
        $customerSlug = $invoice->getCustomer()->slug;
        if (!is_dir(temp_path($customerSlug))) {
            $this->mkdir(temp_path($customerSlug));
        }

        $targetPath = temp_path($customerSlug.'/'.$invoice->getInvoiceNumberSlug().'.pdf');
        copy($path, $targetPath);

        return $targetPath;
    }

    private function mkdir($dir)
    {
        if (!mkdir($dir) && !is_dir($dir)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $dir));
        }
    }
}