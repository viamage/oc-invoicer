<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/27/17
 * Time: 2:18 PM
 */

namespace Viamage\Invoicer\Classes;

use Keios\PaymentGateway\ValueObjects\Cart;
use Keios\PaymentGateway\ValueObjects\Details;
use Keios\ProUser\Models\User;
use Viamage\Invoicer\Contracts\CartStoreInterface;

/**
 * Class CartManager
 * @package Viamage\Invoicer\Classes
 */
class CartManager
{
    /**
     * @var \Viamage\Invoicer\Contracts\CartStoreInterface
     */
    protected $store;

    /**
     * @var \Keios\PaymentGateway\ValueObjects\Cart
     */
    protected $cart;

    /**
     * @var InvoiceBasedProductFactory
     */
    protected $productFactory;

    /**
     * CartManager constructor.
     *
     * @param CartStoreInterface         $store
     * @param InvoiceBasedProductFactory $productFactory
     */
    public function __construct(CartStoreInterface $store, InvoiceBasedProductFactory $productFactory)
    {
        $this->store = $store;
        $this->productFactory = $productFactory;
        $this->cart = $this->store->load();
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->cart->getItemCount();
    }

    /**
     * @return \Keios\PaymentGateway\ValueObjects\Cart
     */
    public function getCart()
    {
        return $this->cart;
    }

    /**
     * @param array $ids
     */
    public function removeProducts(array $ids)
    {
        foreach ($ids as $id) {
            $this->cart->remove($id);
        }

        $this->store->save($this->cart);
    }

    /**
     * @param $invoiceItem
     * @param $currency
     * @return bool
     *
     */
    public function addProduct($invoiceItem, $currency)
    {
        $item = $this->productFactory->convertInvoiceItemToItem($invoiceItem, $currency);
        $this->cart->add($item);
        $this->store->save($this->cart);
        return true;
    }

    /**
     * @param string $productId
     * @throws \Keios\PaymentGateway\Exceptions\ItemNotFoundException
     */
    public function addPiece($productId)
    {
        $this->cart->addPieceToItem($productId);
        $this->store->save($this->cart);
    }

    /**
     * @param string $productId
     * @throws \Keios\PaymentGateway\Exceptions\ItemNotFoundException
     */
    public function subtractPiece($productId)
    {
        $this->cart->subtractPieceOfItem($productId);
        $this->store->save($this->cart);
    }

    /**
     * @param User       $user
     * @param bool|false $asQuote
     *
     * @return string
     * @throws EmptyOrderException
     */
    public function makeOrder(User $user, $asQuote = false)
    {
        if ($this->cart->isEmpty()) {
            throw new EmptyOrderException();
        }

        $details = new Details([], null, null, null, $user->email);

        $hashId = \OrderManager::create($this->cart, $details, $user, $asQuote);

        $this->store->delete();

        return $hashId;
    }

    /**
     * @param User $user
     *
     * @return string
     * @throws \Exception
     */
    public function makeQuote(User $user)
    {

        $details = new Details([], null, null, null, $user->email);

        return \OrderManager::create(new Cart(), $details, $user, true);
    }
}
