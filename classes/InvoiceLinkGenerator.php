<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 10/2/17
 * Time: 1:51 PM
 */

namespace Viamage\Invoicer\Classes;

use ApplicationException;
use Cms\Classes\Theme;
use Viamage\Invoicer\Models\Invoice;

class InvoiceLinkGenerator
{
    private $componentPageCache;

    /**
     * @param Invoice $model
     *
     * @return string
     * @throws \ApplicationException
     * @throws \Exception
     */
    public function getInvoiceLink($model): string
    {
        $component = 'vi_invoice';
        $pages = Theme::getActiveTheme()->listPages();

        if (isset($this->componentPageCache[$component])) {
            return $this->componentPageCache[$component];
        }

        /**
         * @var \Cms\Classes\Page $page
         */
        foreach ($pages as $page) {
            if ($page->hasComponent($component)) {
                $this->componentPageCache[$component] = $page;
                $url = $page->getAttribute('url');
                $url = str_replace(':id', $model->id, $url);
                $url .= '?key='.$model->hash_id;

                return $url;
            }
        }
        throw new ApplicationException('Assign vi_invoice component to some page');
    }

    public function getInvoiceInternalLink($model): string
    {
        return \Backend::url('viamage/invoicer/invoices/preview/'.$model->id);
    }
}