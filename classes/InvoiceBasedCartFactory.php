<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/27/17
 * Time: 2:30 PM
 */

namespace Viamage\Invoicer\Classes;

use Viamage\Invoicer\Models\Invoice;

/**
 * Class InvoiceBasedCartFactory
 * @package Viamage\Invoicer\Classes
 */
class InvoiceBasedCartFactory
{
    /**
     * @var CartManager
     */
    public $cartManager;

    /**
     * @var Invoice
     */
    protected $invoice;

    /**
     * InvoiceBasedCartFactory constructor.
     * @param CartManager $cartManager
     * @param Invoice     $invoice
     */
    public function __construct(CartManager $cartManager, Invoice $invoice)
    {
        $this->cartManager = $cartManager;
        $this->invoice = $invoice;
    }

    /**
     *
     */
    public function setupCartWithInvoice()
    {
        $this->cartManager->getCart()->clear();
        $invoiceItems = $this->invoice->goods;
        foreach ($invoiceItems as $invoiceItem) {
            $this->cartManager->addProduct($invoiceItem, $this->invoice->getCurrency());
        }

        return $this->cartManager;
    }
}