<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 9/28/17
 * Time: 5:32 PM
 */

namespace Viamage\Invoicer\Classes;

use Carbon\Carbon;
use Keios\MoneyRight\Money;
use Keios\MoneySupport\Classes\CurrencyList;
use October\Rain\Exception\ApplicationException;
use PHPExcel_IOFactory;
use SimpleExcel\SimpleExcel;
use Viamage\Invoicer\Contracts\CurrencyConverter;
use Viamage\Invoicer\Models\CurrencyRatio;
use Viamage\Invoicer\Models\Settings;

/**
 * Class IrishCurrencyConverter
 * @package Viamage\Invoicer\Classes
 */
class IrishCurrencyConverter implements CurrencyConverter
{
    /**
     *
     */
    const xlsLink = 'https://www.centralbank.ie/docs/default-source/statistics/interest-rates-exchange-rates/exchange-rates/past-5-days.xls?sfvrsn=110';

    /**
     * @var \PHPExcel_Reader_IReader
     */
    public $excelReader;
    /**
     * @var
     */
    public $excelFile;

    /**
     * IrishCurrencyConverter constructor.
     * @throws \PHPExcel_Reader_Exception
     */
    public function __construct()
    {
        $fileType = 'Excel5';
        $this->excelReader = PHPExcel_IOFactory::createReader($fileType);

    }

    public function convert(Money $amount, $targetIso, Carbon $date = null)
    {
        $now = Carbon::now();
        $sourceIso = $amount->getCurrency()->getIsoCode();
        $dateFormatted = $now->format('d-m-Y');
        $offlineRatio = null;
        if ($date) {
            $offlineRatio = CurrencyRatio::where('date', $date)->first();
            $dateFormatted = $date->format('d-m-Y');
        }
        if ($offlineRatio) {
            $ratios = json_decode($offlineRatio->ratios, true);
        } else {
            /** @var CurrencyConverter $converter */
            $converter = \App::make(CurrencyConverter::class);
            $ratios = $converter->readFor($dateFormatted);
        }

        $ratios['EUR'] = 1.00;
        if (!array_key_exists($sourceIso, $ratios)) {
            throw new ApplicationException('Currency ' . $sourceIso . ' is not supported');
        }
        if (!array_key_exists($targetIso, $ratios)) {
            throw new ApplicationException('Currency ' . $targetIso . ' is not supported');
        }
        $ratio = $ratios[$sourceIso];
        $eur = $amount->getAmountString() * $ratio;
        $result = $eur * $ratios[$targetIso];

        return Money::$targetIso($result);
    }

    /**
     *
     * @throws \PHPExcel_Reader_Exception
     * @throws \PHPExcel_Exception
     * @throws \October\Rain\Exception\ApplicationException
     */
    public function readLatest(): array
    {
        $sheet = $this->loadSheet();
        $selectedDate = Carbon::now()->subDay(1)->format('d M y');

        return $this->getDataForDate($sheet, $selectedDate);
    }

    /**
     * @param $date
     *
     * @return array
     * @throws \October\Rain\Exception\ApplicationException
     * @throws \PHPExcel_Reader_Exception
     */
    public function readFor($date): array
    {
        $sheet = $this->loadSheet();
        $carbon = new Carbon($date);
        $carbon->subDay(1);
        if ($carbon->dayOfWeek === Carbon::SUNDAY) {
            $carbon->subDay(2);
        }
        if ($carbon->dayOfWeek === Carbon::SATURDAY) {
            $carbon->subDay(1);
        }
        $selectedDate = $carbon->format('d M y');

        return $this->getDataForDate($sheet, $selectedDate);
    }

    /**
     * @return array
     * @throws \PHPExcel_Reader_Exception
     * @throws \October\Rain\Exception\ApplicationException
     * @throws ApplicationException
     */
    private function loadSheet(): array
    {
        $this->syncRatesFile();
        if (!$this->excelFile) {
            return [];
        }
        $sheet = $this->excelFile->getSheet()->toArray();
        if (\count($sheet) < 1) {
            throw new ApplicationException('Invalid XLS, something really bad happened');
        }

        return $sheet;
    }

    /**
     * Syncs rates file
     * @throws \PHPExcel_Reader_Exception
     */
    private function syncRatesFile()
    {
        $settings = Settings::instance();
        $context = [
            'ssl' => [
                'verify_peer'      => !$settings->ignore_ssl_issues,
                'verify_peer_name' => !$settings->ignore_ssl_issues,
            ],
        ];
        $filename = 'current_rates.xls';
        $downloadedFile = temp_path($filename);
        $now = Carbon::now()->format('dMY');
        $created = null;
        if (file_exists($downloadedFile)) {
            $created = Carbon::createFromTimestamp(filemtime($downloadedFile))->format('dMY');
        }
        try {
            if ($created !== $now) {
                copy(self::xlsLink, $downloadedFile, stream_context_create($context));
            }
            $this->excelFile = $this->excelReader->load($downloadedFile);
        } catch (\Exception $e) {
            \Flash::error('Cannot connect to Ireland Central Bank! You can issue EUR invoices only');
        }
    }

    /**
     * @param $sheet
     * @param $selectedDate
     *
     * @return array
     * @throws \October\Rain\Exception\ApplicationException
     * @throws ApplicationException
     */
    private function getDataForDate(array $sheet, string $selectedDate): array
    {
        $namedResults = [];
        if (\count($sheet) === 0) {
            return [];
        }
        /** @var array $titles */
        $titles = $sheet[0];
        $valueColumnKey = null;
        $wantedDate = new Carbon($selectedDate);
        foreach ($titles as $key => $title) {
            if ($title === null) {
                unset($titles[$key]);
                continue;
            }
            $titleDate = new Carbon($title);
            if ($titleDate->toDateString() === $wantedDate->toDateString()) {
                $valueColumnKey = $key;
            }
        }

        if (!$valueColumnKey) {
            end($titles);
            $valueColumnKey = key($titles);
        }
        if (!$valueColumnKey) {
            throw new ApplicationException('Ratio not found in data from last 5 days.');
        }
        unset($sheet[0]);
        foreach ($sheet as $value) {
            if ($value[0] && $value[$valueColumnKey]) {
                $namedResults[$value[0]] = $value[$valueColumnKey];
            }
        }

        $results = [];
        $currencies = array_flip(CurrencyList::all());

        foreach ($namedResults as $name => $value) {
            $name = ucwords($name);
            if (array_key_exists($name, $currencies)) {
                $results[$currencies[$name]] = $value;
            }
        }

        return $results;
    }
}
