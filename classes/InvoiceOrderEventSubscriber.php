<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/27/17
 * Time: 2:42 PM
 */

namespace Viamage\Invoicer\Classes;

use Keios\PaymentGateway\Core\Order;
use Keios\PaymentGateway\Core\OrderEventSubscriber;
use Keios\PaymentGateway\Events\CustomerCreatedOrder;
use Keios\PaymentGateway\Events\EmployeeCreatedOrder;
use Keios\PaymentGateway\Events\OrderCanceled;
use Keios\PaymentGateway\Events\OrderCanceledNotActive;
use Keios\PaymentGateway\Events\OrderDeferredRefund;
use Keios\PaymentGateway\Events\OrderDirectRefund;
use Keios\PaymentGateway\Events\OrderFinished;
use Keios\PaymentGateway\Events\OrderGrantedRefund;
use Keios\PaymentGateway\Events\OrderGuaranteeRefund;
use Keios\PaymentGateway\Events\OrderPaid;
use Keios\PaymentGateway\Events\OrderPlaced;
use Keios\PaymentGateway\Events\OrderPurchased;
use Keios\PaymentGateway\Events\OrderRefundedAfterReturn;
use Keios\PaymentGateway\Events\OrderRejected;
use Keios\PaymentGateway\Events\OrderRejectedReceivedPayment;
use Keios\PaymentGateway\Events\OrderRequestedRefund;
use Keios\PaymentGateway\Events\OrderShipped;
use Keios\PaymentGateway\Events\OrderUnderway;
use Keios\PaymentGateway\Events\OrderWaitingForReturn;
use Keios\PaymentGateway\ValueObjects\Details;
use Viamage\Invoicer\Models\Invoice;

/**
 * Class PaymentEventSubscriber
 *
 * @package Keios\PaymentGateway
 */
class InvoiceOrderEventSubscriber extends OrderEventSubscriber
{

    public function onOrderPlaced(OrderPlaced $event)
    {
        /** @var Order $order */
        $order = $event->getObject();
        /** @var Details $details */
        $details = $order->details;
        if ($details->has('invoiceId')) {
            $invoice = Invoice::where('id', $details->get('invoiceId'))->first();
            $invoice->order_id = $order->id;
            $invoice->save();
        }
    }

    public function onOrderPurchased(OrderPurchased $event)
    {
    }

    public function onOrderUnderway(OrderUnderway $event)
    {
    }

    public function onOrderRejected(OrderRejected $event)
    {
    }

    public function onOrderRequestedRefund(OrderRequestedRefund $event)
    {
    }

    public function onOrderCanceled(OrderCanceled $event)
    {
    }

    public function onCanceledNotActive(OrderCanceledNotActive $event)
    {
    }

    public function onOrderPaid(OrderPaid $event)
    {
        /** @var Order $order */
        $order = $event->getObject();
        /** @var Details $details */
        $details = $order->details;
        if ($details->has('invoiceId')) {
            $invoice = Invoice::where('id', $details->get('invoiceId'))->first();
            $invoice->is_paid = true;
            $invoice->order_id = $order->id;
            $invoice->payment_id = $order->successful_payment->id;
            $invoice->save();
        }
    }

    public function onOrderShipped(OrderShipped $event)
    {
    }

    public function onOrderFinished(OrderFinished $event)
    {
    }

    public function onOrderDirectRefund(OrderDirectRefund $event)
    {
    }

    public function onOrderGrantedRefund(OrderGrantedRefund $event)
    {
    }

    public function onOrderWaitingForReturn(OrderWaitingForReturn $event)
    {
    }

    public function onOrderRefundedAfterReturn(OrderRefundedAfterReturn $event)
    {
    }

    public function onOrderDeferredRefund(OrderDeferredRefund $event)
    {
    }

    public function onOrderGuaranteeRefund(OrderGuaranteeRefund $event)
    {
    }

    public function onOrderRejectedReceivedPayment(OrderRejectedReceivedPayment $event)
    {
    }

    public function onCustomerCreatedOrder(CustomerCreatedOrder $event)
    {
    }

    public function onEmployeeCreatedOrder(EmployeeCreatedOrder $event)
    {
    }
}