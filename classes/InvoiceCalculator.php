<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 9/28/17
 * Time: 4:52 PM
 */

namespace Viamage\Invoicer\Classes;

use Viamage\Invoicer\ValueObjects\Good;
use Viamage\Invoicer\ValueObjects\Totals;

/**
 * Class InvoiceCalculator
 * @package Viamage\Invoicer\Classes
 */
class InvoiceCalculator
{
    /**
     * @param array  $goods
     * @param string $currency
     * @return Totals
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public static function countTotals(array $goods, string $currency = 'EUR'): Totals
    {
        $totals = new Totals($currency);
        
        /** @var Good $good */
        foreach ($goods as $good) {
            $totals->totalNet = $totals->totalNet->add($good->totalNetPrice);
            $totals->totalTax = $totals->totalTax->add($good->totalTaxAmount);
            $totals->totalEurNet = $totals->totalEurNet->add($good->totalEurNetPrice);
            $totals->totalEurTax = $totals->totalEurTax->add($good->totalEurTaxAmount);
        }
        $totals->totalGross = $totals->totalNet->add($totals->totalTax);
        $totals->totalEurGross = $totals->totalEurNet->add($totals->totalEurTax);

        return $totals;
    }
}