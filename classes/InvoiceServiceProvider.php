<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/27/17
 * Time: 2:18 PM
 */

namespace Viamage\Invoicer\Classes;

use Illuminate\Support\ServiceProvider;

class InvoiceServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $app = $this->app;

        $this->app->singleton('invoicer.cartmanager', function () use ($app) {
            $sessionCartStore = new SessionCartStore($app->make('session'));
            $productFactory = new InvoiceBasedProductFactory();
            return new CartManager($sessionCartStore, $productFactory);
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [
            'invoicer.cartmanager'
        ];
    }
}