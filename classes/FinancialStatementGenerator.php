<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/20/17
 * Time: 1:12 PM
 */

namespace Viamage\Invoicer\Classes;

use Viamage\Invoicer\Models\AccountantsReport;
use Viamage\Invoicer\Models\DirectorsReport;
use Viamage\Invoicer\Models\DirectorsStatement;
use Viamage\Invoicer\Models\FinancialStatement;
use Viamage\Invoicer\Models\FinancialStatementTemplate;
use Viamage\Invoicer\ValueObjects\Company;

/**
 * Class FinancialStatementGenerator
 * @package Viamage\Invoicer\Classes
 */
class FinancialStatementGenerator
{
    /**
     * @var DirectorsStatement[]
     */
    public $directorsStatements;
    /**
     * @var DirectorsReport
     */
    public $directorsReport;
    /**
     * @var AccountantsReport
     */
    public $accountantsReport;

    /**
     *
     */
    public function generateIndex()
    {
        // todo
    }

    /**
     * @param FinancialStatementTemplate $template
     * @param string|int                 $year
     * @return string
     * @throws \ApplicationException
     */
    public function generate(FinancialStatementTemplate $template, $year)
    {
        $this->setupYear($year);

        $version = 1;
        $exists = FinancialStatement::where('year', $year)->count();
        if ($exists > 0) {
            $version += $exists;
        }

        $statement = new FinancialStatement();
        $statement->year = $year;
        $statement->version = $version;
        $statement->information = ''; // todo
        $statement->directors_report_id = $this->directorsReport->id;
        $statement->accountants_report_id = $this->accountantsReport->id;
        $statement->profit_and_loss = ''; //todo
        $statement->balance_sheet = ''; //todo
        $statement->notes = ''; //todo
        $statement->save();

        foreach ($this->directorsStatements as $directorsStatement) {
            \DB::table('viamage_invoicer_fs_ds')->insert(
                [
                    'director_statement_id' => $directorsStatement->id,
                    'financial_report_id'   => $statement->id,
                ]
            );
        }

        // TODO - at this stage we should get the page with FS component, count pages and generate index somehow. then attach index to model and save.

        return $statement;

    }

    /**
     * @param $statementId
     * @param $templateId
     * @return string
     */
    public function getHtml($statementId, $templateId)
    {
        $template = FinancialStatementTemplate::where('id', $templateId)->first();

        $statement = FinancialStatement::where('id', $statementId)->first();
        $this->setupYearFromStatement($statement);
        $company = new Company();
        $company->fromSettings();
        $vars = [
            'directors_report'     => $this->directorsReport->getContent(),
            'accountants_report'   => $this->accountantsReport->getContent(),
            'report_index'         => '',
            'directors_statements' => [],
            'company'              => $company,
            'report'               => $statement,
        ];
        foreach ($this->directorsStatements as $directorsStatement) {
            $vars['directors_statements'][] = $directorsStatement->getContent();
        }

        return \Twig::parse($template->getContent(), $vars);
    }

    /**
     * @param FinancialStatement $statement
     */
    private function setupYearFromStatement(FinancialStatement $statement)
    {
        $this->directorsReport = $statement->directorsReport;
        $this->directorsStatements = $statement->directorsStatements;
        $this->accountantsReport = $statement->accountantsReport;
    }

    /**
     * @param $year
     * @throws \ApplicationException
     */
    private function setupYear($year)
    {
        $this->directorsReport = DirectorsReport::where('year', $year)->first();
        if (!$this->directorsReport) {
            throw new \ApplicationException('Missing Directors Report for '.$year);
        }
        $this->directorsStatements = DirectorsStatement::where('year', $year)->get();
        if (!$this->directorsStatements) {
            throw new \ApplicationException('Missing Directors Statements for '.$year);
        }
        $this->accountantsReport = AccountantsReport::where('year', $year)->first();
        if (!$this->accountantsReport) {
            throw new \ApplicationException('Missing Accountants Report for '.$year);
        }
    }

    public function getNumPagesInPDF(array $arguments = [])
    {
        @list($PDFPath) = $arguments;
        $stream = @fopen($PDFPath, 'rb');
        $PDFContent = @fread($stream, filesize($PDFPath));
        if (!$stream || !$PDFContent) {
            return false;
        }

        $firstValue = 0;
        $secondValue = 0;
        if (preg_match("/\/N\s+(\d+)/", $PDFContent, $matches)) {
            $firstValue = $matches[1];
        }

        if (preg_match_all("/\/Count\s+(\d+)/s", $PDFContent, $matches)) {
            $secondValue = max($matches[1]);
        }

        return (($secondValue !== 0) ? $secondValue : max($firstValue, $secondValue));
    }
}