<?php

namespace Viamage\Invoicer\Jobs;

use Carbon\Carbon;
use dawood\phpChrome\Chrome;
use File;
use Illuminate\Queue\InteractsWithQueue;
use Keios\Apparatus\Classes\JobManager;
use Keios\Apparatus\Contracts\ApparatusQueueJob;
use URL;
use Viamage\Invoicer\Classes\InvoiceLinkGenerator;
use Viamage\Invoicer\Contracts\InvoiceModel;
use Viamage\Invoicer\Models\Invoice;
use Viamage\Invoicer\Models\Settings;

class GeneratePdf implements ApparatusQueueJob
{
    use InteractsWithQueue;

    const PAGE_SIZE = 1;

    private $jobManager;

    private $storagePath;

    private $invoiceId;

    public $jobId;

    /**
     * Create a new job instance.
     * @param int $invoiceId
     * @internal param Invoice $invoice
     */
    public function __construct(int $invoiceId)
    {
        $now = Carbon::now();
        $this->storagePath = storage_path('pdf').'/'.$now->format('Y');
        $this->invoiceId = $invoiceId;
    }

    /**
     * Execute the job.
     *
     * @param JobManager $jobManager
     * @return void
     * @throws \ApplicationException
     */
    public function handle(JobManager $jobManager)
    {
        $this->jobManager = $jobManager;
        $this->jobManager->startJob($this->jobId, 1);
        try {
            if ($this->jobManager->checkIfCanceled($this->jobId)) {
                $this->jobManager->cancelJob($this->jobId);
            } else {
                /** @var Invoice $invoice */
                $invoice = Invoice::where('id', $this->invoiceId)->first();
                if (!$invoice) {
                    throw new \ApplicationException('Invoice does not exist!');
                }
                $settings = Settings::instance();
                $chromePath = $settings->get('chrome_path');
                $fullFilePath = $this->storagePath.'/'.$invoice->getInvoiceNumberSlug().'.pdf';
                if (!@mkdir($this->storagePath) && !is_dir($this->storagePath)) {
                    throw new \ApplicationException('Cannot create directory for invoices. Check permissions.');
                }

                if (!$chromePath) {
                    throw new \ApplicationException('No chrome path configured in settings');
                }
                /** @var InvoiceLinkGenerator $linkGenerator */
                $linkGenerator = \App::make(InvoiceLinkGenerator::class);
                $url = URL::to($linkGenerator->getInvoiceLink($invoice));
                if(File::exists($fullFilePath)){
                    unlink($fullFilePath);
                }
                $command = $chromePath.' --headless --disable-gpu --incognito --enable-viewport --window-size=1477,768 --print-to-pdf='.$fullFilePath.' "'.$url.'"';
                exec($command);
                if (!File::exists($fullFilePath)) {
                    throw new \ApplicationException(
                        'PDF was not generated properly. Contact support or check Google Chrome installation on server'
                    );
                }
                $invoice->pdf = $fullFilePath;
                $invoice->save();
                $metadata = $this->jobManager->getMetadata($this->jobId);
                /** @var \System\Models\File $pdf */
                $pdf = $invoice->pdf;
                $metadata['File Link'] = $pdf->getPath();
                $this->jobManager->completeJob($this->jobId, $metadata);
            }
        } catch (\Exception $e) {
            \Log::error('Job '.$this->jobId.' crashed with '.$e->getMessage().' '.$e->getTraceAsString());
            $meta = $this->jobManager->getMetadata($this->jobId);
            $meta['error'] = $e->getMessage();
            $this->jobManager->failJob($this->jobId, $meta);
        }
    }

    public function assignJobId(int $id)
    {
        $this->jobId = $id;
    }
}
