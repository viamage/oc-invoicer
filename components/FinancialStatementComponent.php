<?php namespace Viamage\Invoicer\Components;

use Cms\Classes\ComponentBase;
use Viamage\Invoicer\Classes\FinancialStatementGenerator;

class FinancialStatementComponent extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name'        => 'viamage.invoicer::lang.components.financialstatementcomponent.name',
            'description' => 'viamage.invoicer::lang.components.financialstatementcomponent.description'
        ];
    }

    public function defineProperties()
    {
        return [
            'fsId' => [
                'title'       => 'Invoice ID',
                'description' => 'ID of the Invoice',
                'default'     => '{{ :fsId }}',
                'type'        => 'string',
            ],
            'templateId' => [
                'title'       => 'Invoice ID',
                'description' => 'ID of the Invoice',
                'default'     => '{{ :templateId }}',
                'type'        => 'string',
            ],
        ];
    }

    public function onRun()
    {
        $fsId = $this->property('fsId');
        $templateId = $this->property('templateId');
        $generator = new FinancialStatementGenerator();
        $this->page['financial_report'] = $generator->getHtml($fsId, $templateId);
    }

}