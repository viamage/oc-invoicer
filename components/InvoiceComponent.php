<?php namespace Viamage\Invoicer\Components;

use Backend\Classes\AuthManager;
use Cms\Classes\ComponentBase;
use Cms\Classes\Theme;
use Keios\MoneySupport\Classes\CurrencyList;
use Keios\PaymentGateway\Core\OrderComponentResolver;
use Keios\PaymentGateway\Facades\OrderManager;
use Keios\PaymentGateway\ValueObjects\Details;
use Redirect;
use System\Models\File;
use Viamage\Invoicer\Classes\CartManager;
use Viamage\Invoicer\Classes\InvoiceBasedCartFactory;
use Viamage\Invoicer\Classes\InvoiceCalculator;
use Viamage\Invoicer\Classes\NumberConverter;
use Viamage\Invoicer\Models\Invoice;
use Viamage\Invoicer\Models\Settings;
use Viamage\Invoicer\ValueObjects\Company;
use Viamage\Invoicer\ValueObjects\Good;

/**
 * Class InvoiceComponent
 * @package Viamage\Invoicer\Components
 */
class InvoiceComponent extends ComponentBase
{
    /**
     * @return array
     */
    public function componentDetails(): array
    {
        return [
            'name'        => 'InvoiceComponent Component',
            'description' => 'No description provided yet...',
        ];
    }

    /**
     * @return array
     */
    public function defineProperties(): array
    {
        return [
            'id' => [
                'title'       => 'Invoice ID',
                'description' => 'ID of the Invoice',
                'default'     => '{{ :id }}',
                'type'        => 'string',
            ],
        ];
    }

    /**
     * @var array
     */
    public $componentPageCache;

    /**
     * @return \Illuminate\Http\RedirectResponse
     * @throws \ValidationException
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function onRun()
    {
        $auth = false;
        $data = \Input::all();
        $id = $this->property('id');
        $invoiceModel = Invoice::where('id', $id)->where('is_displayed', true)->first();
        if (!$invoiceModel) {
            return \Redirect::to('/404');
        }
        if (array_key_exists('key', $data) && $invoiceModel->hash_id === $data['key']) {
            $auth = true;
        }
        if (!$auth) {
            /** @var AuthManager $auth */
            $auth = AuthManager::instance();
            $user = $auth->getUser();
            if (!$user->is_superuser || !$user->hasPermission('viamage.invoicer.access_invoices')) {
                return \Redirect::to('/404');
            }
        }
        $settings = Settings::instance();
        $issuer = new Company();
        $client = new Company();
        $issuer->fromSettings();
        $client->fromModel($invoiceModel->customer);
        $goods = $this->getGoodsFor($invoiceModel);
        $numberConverter = new NumberConverter();
        $totals = InvoiceCalculator::countTotals($goods, $invoiceModel->currency);
        $this->page['issuer'] = $issuer;
        $this->page['client'] = $client;
        $this->page['invoice'] = $invoiceModel;
        $this->page['goods'] = $goods;
        $this->page['totals'] = $totals;
        if ($invoiceModel->order) {
            $this->page['order_url'] = $this->getUrlForOrder($invoiceModel->order->hash_id);
        }
        $this->page['bankTransfer'] = $this->prepareBankDetails();
        $this->page['totalWords'] = ucfirst(
            $numberConverter->convertNumber($totals->totalGross->getAmountBasic()).' '.CurrencyList::findName(
                $invoiceModel->currency
            )
        );
        $this->page['totalEurWords'] = ucfirst(
            $numberConverter->convertNumber($totals->totalEurGross->getAmountBasic()).' Euro'
        );
    }

    /**
     * @param Invoice $model
     * @return array
     * @throws \ValidationException
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function getGoodsFor(Invoice $model): array
    {
        $result = [];
        foreach ($model->goods as $good) {
            $goodVo = new Good();
            $goodVo->fromModelEntry($good, $model->ratio, $model->currency);
            $result[] = $goodVo;
        }

        return $result;
    }

    public function onPayInvoice()
    {
        $input = \Input::all();
        if (!array_key_exists('key', $input)) {
            throw new \ApplicationException('Invalid Invoice');
        }
        $key = $input['key'];
        /** @var Invoice $invoice */
        $invoice = Invoice::where('hash_id', $key)->first();

        if (!$invoice) {
            return null;
        }
        /** @var CartManager $cartManager */
        $cartManager = \App::make('invoicer.cartmanager');
        $cartFactory = new InvoiceBasedCartFactory($cartManager, $invoice);
        $cartFactory->setupCartWithInvoice();
        $cart = $cartManager->getCart();
        $count = $cart->getItemCount();
        $customer = $invoice->getCustomer();
        $email = null;
        if ($customer) {
            $email = $customer->email;
        }
        if ($count > 0) {
            $returnUrl = $this->property('returnPage');
            $description = $invoice->getInvoiceNumber();
            $details = new Details(
                [
                    'invoiceNumber' => $invoice->getInvoiceNumber(),
                    'invoiceId'     => $invoice->id,
                ], null, $description, $returnUrl, $email
            );
            $hashId = OrderManager::create($cart, $details);
            $resolver = \App::make('paymentgateway.ordercomponentresolver');
            $cart->clear();

            return Redirect::to($resolver->resolveFor($hashId));
        }

        return null;
    }

    public function getUrlForOrder($hashId)
    {
        /** @var OrderComponentResolver $resolver */
        $resolver = \App::make('paymentgateway.ordercomponentresolver');

        return $resolver->resolveFor($hashId);
    }

    private function prepareBankDetails()
    {
        $settings = \Keios\PaymentGateway\Models\Settings::instance();
        $data = [];
        $data['name'] = $settings->get('keios.paymentgateway::lang.channels.manualTransfer.name');
        $data['receiver'] = $settings->get('keios.paymentgateway::lang.channels.manualTransfer.receiverAddress');
        $data['bank'] = $settings->get('keios.paymentgateway::lang.channels.manualTransfer.bankDetails');
        $data['swift'] = $settings->get('keios.paymentgateway::lang.channels.manualTransfer.swift');
        $data['iban'] = $settings->get('keios.paymentgateway::lang.channels.manualTransfer.bankAccNo');

        return $data;
    }

}
