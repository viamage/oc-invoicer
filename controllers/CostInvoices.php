<?php namespace Viamage\Invoicer\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Viamage\Invoicer\Models\CostInvoice;

/**
 * Cost Invoices Back-end Controller
 */
class CostInvoices extends Controller
{
    /**
     * @var array
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';
    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';

    /**
     * CostInvoices constructor.
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Viamage.Invoicer', 'invoicer', 'costinvoices');
    }

    /**
     * Deleted checked costinvoices.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && \is_array($checkedIds) && \count($checkedIds)) {

            foreach ($checkedIds as $costinvoiceId) {
                if (!$costinvoice = CostInvoice::find($costinvoiceId)) {
                    continue;
                }
                $costinvoice->delete();
            }

            Flash::success(Lang::get('viamage.invoicer::lang.costinvoices.delete_selected_success'));
        } else {
            Flash::error(Lang::get('viamage.invoicer::lang.costinvoices.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
