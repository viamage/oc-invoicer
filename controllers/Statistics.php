<?php namespace Viamage\Invoicer\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Carbon\Carbon;
use Flash;
use Keios\MoneyRight\Money;
use Lang;
use Viamage\Invoicer\Models\CostInvoice;
use Viamage\Invoicer\Models\Invoice;
use Viamage\Invoicer\Models\Settings;
use Viamage\Invoicer\Models\Statistic;

/**
 * Statistics Back-end Controller
 */
class Statistics extends Controller
{
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Viamage.Invoicer', 'invoicer', 'statistics');
    }

    public $implement = [
        'Backend.Behaviors.ImportExportController',
    ];

    public $importExportConfig = 'config_import_export.yaml';


    public function index()
    {
        $now = Carbon::now();
        $settings = Settings::instance();
        $this->pageTitle = 'Payments Statistics';
        $foundation = new Carbon($settings->get('company_foundation_date', '21-09-2017'));
        $ardDay = $foundation->copy()->addMonths(6)->format('d-M');
        $ard = new Carbon($ardDay.'-'.date('Y'));
        if ($now->diffInDays($ard, false) < 0) {
            $ard->addYear();
        }
        $fiscalYearEnd = $ard;
        // check if it's first year
        if($fiscalYearEnd->year === 2019){
            $fiscalYearStart = $foundation;
        } else {
            $fiscalYearStart = $ard->copy()->subYear();
        }
        $outgoing = Invoice::where('issue_date', '>=', $fiscalYearStart)->where(
            'issue_date',
            '<=',
            $fiscalYearEnd
        )->get();

        $incoming = CostInvoice::where('invoice_date', '>=', $fiscalYearStart)->where(
            'invoice_date',
            '<=',
            $fiscalYearEnd
        )->get();
        /** @var Money $profit */
        $incomingUpaid = 0;
        $outgoingUnpaid = 0;
        $incomingCount = \count($incoming);
        $outgoingCount = \count($outgoing);
        $incomingValue = Money::EUR(0);
        $incomingValueGross = Money::EUR(0);
        $outgoingValue = Money::EUR(0);
        $outgoingValueGross = Money::EUR(0);
        $outgoingUnpaidValue = Money::EUR(0);
        $incomingUnpaidValue = Money::EUR(0);
        $zero = Money::EUR(0);
        $cit = Money::EUR(0);
        $positiveProfit = true;
        $positiveCit = true;
        $invoices = [];
        /** @var CostInvoice $incomingInvoice */
        foreach ($incoming as $incomingInvoice) {
            $netValue = $incomingInvoice->getEurNet();
            $grossValue = $incomingInvoice->getEurGross();
            $incomingValue = $incomingValue->add(Money::EUR($netValue));
            $incomingValueGross = $incomingValueGross->add(Money::EUR($grossValue));
            $issue = new Carbon($incomingInvoice->getIssueDate());
            $key = $issue->format('Ymdhms').$incomingInvoice->created_at->format('Ymdhms');
            $invoices[$key] = $incomingInvoice;
            if(!$incomingInvoice->is_paid){
                ++$incomingUpaid;
                $incomingUnpaidValue = $incomingUnpaidValue->add(Money::EUR($grossValue));
            }
        }
        /** @var Invoice $outgoingInvoice */
        foreach ($outgoing as $outgoingInvoice) {
            $netValue = $outgoingInvoice->getEurNet();
            $grossValue = $outgoingInvoice->getEurGross();
            $outgoingValue = $outgoingValue->add(Money::EUR($netValue));
            $outgoingValueGross = $outgoingValueGross->add(Money::EUR($grossValue));
            $issue = new Carbon($outgoingInvoice->getIssueDate());
            $key = $issue->format('Ymdhms').$outgoingInvoice->created_at->format('Ymdhms');
            $invoices[$key] = $outgoingInvoice;
            if(!$outgoingInvoice->is_paid){
                ++$outgoingUnpaid;
                $outgoingUnpaidValue = $outgoingUnpaidValue->add(Money::EUR($grossValue));
            }
        }
        krsort($invoices);

        $profit = $outgoingValue->subtract($incomingValue);
        $profitGross = $outgoingValueGross->subtract($incomingValueGross);
        if ($profit->greaterThan($zero)) {
            $positiveProfit = false;
            $positiveCit = false;
            $cit = $profit->multiply(0.125);
        }
        $this->vars['invoices'] = $invoices;
        $this->vars['incomingCount'] = $incomingCount;
        $this->vars['outgoingCount'] = $outgoingCount;
        $this->vars['incomingValue'] = $incomingValue;
        $this->vars['outgoingValue'] = $outgoingValue;
        $this->vars['profit'] = $profit;
        $this->vars['cit'] = $cit;
        $this->vars['positiveProfit'] = $positiveProfit;
        $this->vars['positiveCit'] = $positiveCit;
        $this->vars['profitGross'] = $profitGross;
        $this->vars['incomingUnpaid'] = $incomingUpaid;
        $this->vars['outgoingUnpaid'] = $outgoingUnpaid;
        $this->vars['outgoingUnpaidValue'] = $outgoingUnpaidValue;
        $this->vars['incomingUnpaidValue'] = $incomingUnpaidValue;
    }

}
