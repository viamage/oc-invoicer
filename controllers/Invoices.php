<?php namespace Viamage\Invoicer\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Carbon\Carbon;
use Keios\Apparatus\Classes\JobManager;
use URL;
use Viamage\Invoicer\Classes\InvoiceLinkGenerator;
use Viamage\Invoicer\Classes\IrishCurrencyConverter;
use Viamage\Invoicer\Classes\Mailer;
use Viamage\Invoicer\Contracts\InvoiceModel;
use Viamage\Invoicer\Jobs\GeneratePdf;
use Viamage\Invoicer\Models\Invoice;
use Viamage\Invoicer\Models\Settings;
use Viamage\Invoicer\ValueObjects\Good;

/**
 * Invoices Back-end Controller
 */
class Invoices extends Controller
{
    const due_soon_keyword = 'soon';
    const new_invoice_keyword = 'new';
    const due_passed_keyword = 'passed';
    const unknown_keyword = 'unknown';

    /**
     * @var array
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';
    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';
    /**
     * @var string
     */
    public $relationConfig = 'config_relations.yaml';

    /**
     * Invoices constructor.
     */
    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Viamage.Invoicer', 'invoicer', 'invoices');
    }

    public function onGeneratePdf($modelId)
    {
        /** @var JobManager $jobManager */
        $jobManager = \App::make(JobManager::class);

        /** @var Invoice $model */
        $model = Invoice::where('id', $modelId)->first();
        $job = new GeneratePdf($model->id);
        $metadata = [
            'ID'       => $model->id,
            'Number'   => $model->getInvoiceNumber(),
            'Link'     => url($model->getInvoiceLink()),
            'Internal' => $model->getInvoiceInternalLink(),
        ];
        $properties = [
            'metadata' => $metadata,
            'count'    => 1,
        ];

        $jobId = $jobManager->dispatch($job, 'PDF Generation for '.$model->getInvoiceNumber(), $properties);

        return \Redirect::to(\Backend::url('keios/apparatus/jobs/view/'.$jobId));
    }

    /**
     * @param Invoice $model
     * @return array
     * @throws \ValidationException
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function getGoodsFor(Invoice $model): array
    {
        $result = [];
        foreach ($model->goods as $good) {
            $goodVo = new Good();
            $goodVo->fromModelEntry($good, $model->ratio, $model->currency);
            $result[] = $goodVo;
        }

        return $result;
    }

    public function getInvoiceLink($model)
    {
        $linkGenerator = \App::make(InvoiceLinkGenerator::class);

        return URL::to($linkGenerator->getInvoiceLink($model));
    }

    /**
     * @return bool
     * @throws \ApplicationException
     */
    public function onSendPassingSoonEmail()
    {
        $data = post();
        $model = Invoice::where('id', $data['invoice_id'])->first();
        $mailer = new Mailer();
        try {
            $mailer->sendSoonReminder($model);
            \Flash::success('Email properly sent');
        } catch (\Exception $e) {
            \Log::error($e->getMessage().' '.$e->getTraceAsString());
            \Flash::error($e->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @return bool
     * @throws \ApplicationException
     */
    public function onSendPassedEmail()
    {
        $data = post();
        $model = Invoice::where('id', $data['invoice_id'])->first();
        $mailer = new Mailer();
        try {
            $mailer->sendPassedReminder($model);
            \Flash::success('Email properly sent');
        } catch (\Exception $e) {
            \Log::error($e->getMessage().' '.$e->getTraceAsString());
            \Flash::error($e->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @return bool
     * @throws \ApplicationException
     */
    public function onSendNewInvoice()
    {
        $data = post();
        $model = Invoice::where('id', $data['invoice_id'])->first();
        $mailer = new Mailer();
        try {
            $mailer->sendInvoice($model);
            \Flash::success('Email properly sent');
        } catch (\Exception $e) {
            \Log::error($e->getMessage().' '.$e->getTraceAsString());
            \Flash::error($e->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @param $model
     * @return string
     */
    public function getDueStatus(InvoiceModel $model): string
    {
        $now = Carbon::now();
        $dueDate = $model->due_date;
        $settings = Settings::instance();
        $dueSoon = $settings->get('due_soon', 3);
        $dueDate = new Carbon($dueDate);

        $diff = $now->diffInDays($dueDate, false);
        if ($diff >= 0 && $diff <= $dueSoon) {
            return self::due_soon_keyword;
        }
        if ($diff < 0) {
            return self::due_passed_keyword;
        }

        return self::unknown_keyword;
    }
}
