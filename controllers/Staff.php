<?php namespace Viamage\Invoicer\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Viamage\Invoicer\Models\Staff as StaffModel;

/**
 * Staff Back-end Controller
 */
class Staff extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Viamage.Invoicer', 'invoicer', 'staff');
    }

    /**
     * Deleted checked staff.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && \is_array($checkedIds) && \count($checkedIds)) {

            foreach ($checkedIds as $staffId) {
                if (!$staff = StaffModel::find($staffId)) {
                    continue;
                }
                $staff->delete();
            }

            Flash::success(Lang::get('viamage.invoicer::lang.staff.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('viamage.invoicer::lang.staff.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
