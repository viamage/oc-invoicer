<?php namespace Viamage\Invoicer\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Flash;
use Lang;
use Viamage\Invoicer\Models\DirectorsStatement;

/**
 * Directors Statements Back-end Controller
 */
class DirectorsStatements extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Viamage.Invoicer', 'invoicer', 'directorsstatements');
    }

    /**
     * Deleted checked directorsstatements.
     */
    public function index_onDelete()
    {
        if (($checkedIds = post('checked')) && \is_array($checkedIds) && \count($checkedIds)) {

            foreach ($checkedIds as $directorsstatementId) {
                if (!$directorsstatement = DirectorsStatement::find($directorsstatementId)) {
                    continue;
                }
                $directorsstatement->delete();
            }

            Flash::success(Lang::get('viamage.invoicer::lang.directorsstatements.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('viamage.invoicer::lang.directorsstatements.delete_selected_empty'));
        }

        return $this->listRefresh();
    }
}
