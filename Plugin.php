<?php namespace Viamage\Invoicer;

use Backend;
use Event;
use Form;
use Keios\PaymentGateway\Core\Order;
use Keios\PaymentGateway\Core\SerializationService;
use Keios\PaymentGateway\Facades\ItemFormatter;
use Keios\PaymentGateway\ValueObjects\Cart;
use October\Rain\Database\Model;
use System\Classes\PluginBase;
use Viamage\Invoicer\Classes\{
    InvoiceItem,
    InvoiceItemFormatter,
    InvoiceOrderEventSubscriber,
    InvoiceServiceProvider,
    IrishCurrencyConverter
};
use Viamage\Invoicer\Components\InvoiceComponent;
use Viamage\Invoicer\Console\GeneratePdf;
use Viamage\Invoicer\Console\GetIrishRatios;
use Viamage\Invoicer\Contracts\CurrencyConverter;
use Viamage\Invoicer\Models\Invoice;
use Viamage\Invoicer\Models\Settings;

/**
 * Invoicer Plugin Information File
 */
class Plugin extends PluginBase
{
    const defaultCurrencyConverter = IrishCurrencyConverter::class;

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'Invoicer',
            'description' => 'No description provided yet...',
            'author'      => 'Viamage',
            'icon'        => 'icon-leaf',
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            'invoicer:generate-pdf',
            GeneratePdf::class
        );
        $this->commands(
            [
                'invoicer:generate-pdf',
                GetIrishRatios::class
            ]
        );

        $dispatcher = $this->app['events'];

        /**
         * @var Dispatcher $dispatcher
         */
        $dispatcher->listen(
            'paymentgateway.booted',
            function () {

                ItemFormatter::add(InvoiceItem::TYPE, InvoiceItemFormatter::class);
                ItemFormatter::useForNewQuoteItems(InvoiceItem::class);
            }
        );
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     * @throws \JMS\Serializer\Exception\InvalidArgumentException
     */
    public function boot()
    {
        if (class_exists('Keios\PaymentGateway\Models\Order')) {
            Invoice::extend(
                function (Model $model) {
                    $model->belongsTo['order'] = 'Keios\PaymentGateway\Models\Order';
                    $model->belongsTo['payment'] = 'Keios\PaymentGateway\models\Payment';
                }
            );
            \Keios\PaymentGateway\Models\Order::extend(
                function (Model $model) {
                    $model->hasOne['invoice'] = Invoice::class;
                }
            );
        }

        $this->app['events']->listen(
            'backend.form.extendFields',
            function ($widget) {

                if (!$widget->model instanceof Backend\Models\User) {
                    return;
                }

                $backendUserProfile = [
                    'invoice_prefix' => [
                        'label' => 'Prefix for user in Invoice Number Suggestion',
                        'tab'   => 'Invoicing',
                    ],
                ];

                $contexts = ['update', 'myaccount'];

                if (\in_array($widget->getContext(), $contexts, true)) {
                    $widget->addTabFields($backendUserProfile);
                }
            }
        );

        $settings = Settings::instance();
        $country = $settings->get('country');
        if ($country === 'IE') {
            $this->app->bind(
                CurrencyConverter::class,
                IrishCurrencyConverter::class
            );
        } else {
            $this->app->bind(
                CurrencyConverter::class,
                self::defaultCurrencyConverter
            );
        }

        $this->app->register(InvoiceServiceProvider::class);
        Event::subscribe(new InvoiceOrderEventSubscriber());

        $this->app['events']->listen(
            SerializationService::class,
            function (SerializationService $service) {
                $service->registerMetadataDirectory(
                    plugins_path('viamage/invoicer/metadata'),
                    'Viamage.Invoicer'
                );
            }
        );
    }

    public function registerConsoleCommand($key, $class)
    {

    }

    /**
     * @param $schedule
     */
    public function registerSchedule($schedule)
    {
        $schedule->command('invoicer:get-irish-ratios')->dailyAt('08:00');
    }


    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents(): array
    {
        return [
            InvoiceComponent::class => 'vi_invoice',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions(): array
    {
        return [
            'viamage.invoicer.access_settings'                    => [
                'tab'   => 'Invoicer',
                'label' => 'Access Settings',
            ],
            'viamage.invoicer.access_invoices'                    => [
                'tab'   => 'Invoicer',
                'label' => 'Access Invoices',
            ],
            'viamage.invoicer.access_customers'                   => [
                'tab'   => 'Invoicer',
                'label' => 'Access Customers',
            ],
            'viamage.invoicer.access_staff'                       => [
                'tab'   => 'Invoicer',
                'label' => 'Access Staff',
            ],
            'viamage.invoicer.access_directorsreports'            => [
                'tab'   => 'Invoicer',
                'label' => 'Access Directors Reports',
            ],
            'viamage.invoicer.access_directorsstatements'         => [
                'tab'   => 'Invoicer',
                'label' => 'Access Directors Statements',
            ],
            'viamage.invoicer.access_accountantsreports'          => [
                'tab'   => 'Invoicer',
                'label' => 'Access Accountants Reports',
            ],
            'viamage.invoicer.access_financialstatements'         => [
                'tab'   => 'Invoicer',
                'label' => 'Access Financial Statements',
            ],
            'viamage.invoicer.access_financialstatementtemplates' => [
                'tab'   => 'Invoicer',
                'label' => 'Access Financial Statements Templates',
            ],
        ];
    }

    /**
     * @return array
     */
    public function registerSettings(): array
    {
        return [
            'settings' => [
                'label'       => 'viamage.invoicer::lang.settings.menu_label',
                'description' => 'viamage.invoicer::lang.settings.menu_description',
                'category'    => 'viamage.invoicer::lang.plugin.name',
                'icon'        => 'icon-money',
                'class'       => Models\Settings::class,
                'order'       => 600,
                'permissions' => ['viamage.invoicer.access_settings'],
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation(): array
    {
        return [
            'invoicer' => [
                'label'       => 'viamage.invoicer::lang.labels.invoicer',
                'url'         => Backend::url('viamage/invoicer/statistics'),
                'icon'        => 'icon-money',
                'order'       => 500,
                'permissions' => ['viamage.invoicer.*'],
                'sideMenu'    => [
                    'statistics'                  => [
                        'label'       => 'viamage.invoicer::lang.labels.statistics',
                        'icon'        => 'icon-money',
                        'url'         => Backend::url('viamage/invoicer/statistics'),
                        'permissions' => ['viamage.invoicer.access_invoices'],
                    ],
                    'invoices'                    => [
                        'label'       => 'viamage.invoicer::lang.labels.outgoing_invoices',
                        'icon'        => 'icon-upload',
                        'url'         => Backend::url('viamage/invoicer/invoices'),
                        'permissions' => ['viamage.invoicer.access_invoices'],
                    ],
                    'costinvoices'                => [
                        'label'       => 'viamage.invoicer::lang.labels.incoming_invoices',
                        'icon'        => 'icon-download',
                        'url'         => Backend::url('viamage/invoicer/costinvoices'),
                        'permissions' => ['viamage.invoicer.access_invoices'],
                    ],
                    'customers'                   => [
                        'label'       => 'viamage.invoicer::lang.labels.customers',
                        'icon'        => 'icon-users',
                        'url'         => Backend::url('viamage/invoicer/customers'),
                        'permissions' => ['viamage.invoicer.access_customers'],
                    ],
                    'financialstatements'         => [
                        'label'       => 'viamage.invoicer::lang.labels.financialstatements',
                        'icon'        => 'icon-money',
                        'url'         => Backend::url('viamage/invoicer/financialstatements'),
                        'permissions' => ['viamage.invoicer.access_financialstatements'],
                    ],
                    'staff'                       => [
                        'label'       => 'viamage.invoicer::lang.labels.staff',
                        'icon'        => 'icon-briefcase',
                        'url'         => Backend::url('viamage/invoicer/staff'),
                        'permissions' => ['viamage.invoicer.access_staff'],
                    ],
                    'directorsreports'            => [
                        'label'       => 'viamage.invoicer::lang.labels.directorsreports',
                        'icon'        => 'icon-folder-open',
                        'url'         => Backend::url('viamage/invoicer/directorsreports'),
                        'permissions' => ['viamage.invoicer.access_directorsreports'],
                    ],
                    'directorsstatements'         => [
                        'label'       => 'viamage.invoicer::lang.labels.directorsstatements',
                        'icon'        => 'icon-folder-open-o',
                        'url'         => Backend::url('viamage/invoicer/directorsstatements'),
                        'permissions' => ['viamage.invoicer.access_directorsstatements'],
                    ],
                    'accountantsreports'          => [
                        'label'       => 'viamage.invoicer::lang.labels.accountantsreports',
                        'icon'        => 'icon-line-chart',
                        'url'         => Backend::url('viamage/invoicer/accountantsreports'),
                        'permissions' => ['viamage.invoicer.access_accountantsreports'],
                    ],
                    'financialstatementtemplates' => [
                        'label'       => 'viamage.invoicer::lang.labels.financialstatementtemplates',
                        'icon'        => 'icon-file-o',
                        'url'         => Backend::url('viamage/invoicer/financialstatementtemplates'),
                        'permissions' => ['viamage.invoicer.access_financialstatementtemplates'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function registerMailTemplates(): array
    {
        return [
            'viamage.invoicer::mail.due-reminder' => 'Reminder about upcoming invoice due date.',
            'viamage.invoicer::mail.due-time'     => 'Reminder about passed invoice due date.',
            'viamage.invoicer::mail.new-invoice'  => 'Information about new invoice.',
        ];
    }

    public $loadedYamlConfiguration;

    /**
     * @param boolean $loadedYamlConfigration
     */
    public function setLoadedYamlConfigration($loadedYamlConfigration)
    {
        $this->loadedYamlConfigration = $loadedYamlConfigration;
    }

    /**
     * @param array $require
     */
    public function setRequire($require)
    {
        $this->require = $require;
    }

    /**
     * @param boolean $elevated
     */
    public function setElevated($elevated)
    {
        $this->elevated = $elevated;
    }

    /**
     * @param boolean $disabled
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;
    }

    /**
     * @param \Illuminate\Contracts\Foundation\Application $app
     */
    public function setApp($app)
    {
        $this->app = $app;
    }

    /**
     * @param boolean $defer
     */
    public function setDefer($defer)
    {
        $this->defer = $defer;
    }

    /**
     * @param array $publishes
     */
    public static function setPublishes($publishes)
    {
        self::$publishes = $publishes;
    }

    /**
     * @param array $publishGroups
     */
    public static function setPublishGroups($publishGroups)
    {
        self::$publishGroups = $publishGroups;
    }
}
