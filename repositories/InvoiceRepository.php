<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/27/17
 * Time: 12:30 PM
 */

namespace Viamage\Invoicer\Repositories;

use October\Rain\Database\Builder;
use October\Rain\Database\Relations\HasMany;
use Viamage\Invoicer\Models\Invoice;

class InvoiceRepository
{
    public function getForCustomer($customer)
    {
        return $customer->invoices()->where('customer_id', $customer->id)->where('is_displayed', true)->orderBy(
            'is_paid',
            'ASC'
        )->orderBy('due_date', 'DESC')->get();
    }

    public function getForCustomerPaginated($customer, $perPage = 25)
    {
        /** @var Builder $query */
        $query = Invoice::where('customer_id', $customer->id)->where('customer_id', $customer->id)->where(
            'is_displayed',
            true
        )->orderBy('is_paid', 'ASC')->orderBy('due_date', 'DESC');

        return $query->paginate($perPage, null, ['*'], 'ipage');
    }

    public function getUnpaidForCustomer($customer)
    {
        return $customer->invoices()->where('customer_id', $customer->id)->with('advances')->where('is_paid', false)->get();
    }
}