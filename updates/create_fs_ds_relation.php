<?php namespace Viamage\Invoicer\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateInvoicesTable
 * @package Viamage\Invoicer\Updates
 */
class CreateFsDsRelationn extends Migration
{
    public function up()
    {
        Schema::create(
            'viamage_invoicer_fs_ds',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->integer('director_statement_id');
                $table->integer('financial_report_id');
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('viamage_invoicer_fs_ds');
    }
}
