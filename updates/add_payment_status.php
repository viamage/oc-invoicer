<?php namespace Viamage\Invoicer\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class AddPaymentStatus
 * @package Viamage\Invoicer\Updates
 */
class AddPaymentStatus extends Migration
{
    public function up()
    {
        Schema::table('viamage_invoicer_cost_invoices', function($table)
        {
            $table->boolean('is_paid')->after('total_gross')->default(false);
            $table->string('payment_method')->after('is_paid')->nullable();
        });
        Schema::table('viamage_invoicer_invoices', function($table)
        {
            $table->boolean('is_paid')->after('total_gross_eur')->default(false);
            $table->string('payment_method')->after('is_paid')->nullable();
        });
    }

    public function down()
    {
        Schema::table('viamage_invoicer_cost_invoices', function($table)
        {
            $table->dropColumn('is_paid');
            $table->dropColumn('payment_method');
        });
        Schema::table('viamage_invoicer_invoices', function($table)
        {
            $table->dropColumn('is_paid');
            $table->dropColumn('payment_method');
        });
    }
}
