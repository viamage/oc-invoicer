<?php namespace Viamage\Invoicer\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class AddInvoiceKey
 * @package Viamage\Invoicer\Updates
 */
class AddCurrencyToCustomer extends Migration
{
    public function up()
    {
        Schema::table(
            'viamage_invoicer_customers',
            function (Blueprint $table) {
                $table->text('currency')->after('phone');
            }
        );
    }

    public function down()
    {
        Schema::table(
            'viamage_invoicer_customers',
            function (Blueprint $table) {
                $table->dropColumn('currency');
            }
        );
    }
}
