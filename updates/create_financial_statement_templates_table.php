<?php namespace Viamage\Invoicer\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFinancialStatementTemplatesTable extends Migration
{
    public function up()
    {
        Schema::create(
            'viamage_invoicer_financial_statement_templates',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('title');
                $table->longText('content');
                $table->timestamps();
            }
        );

        \DB::table('viamage_invoicer_financial_statement_templates')->insert([
           'title' => 'Default',
           'content' => $this->getDefaultTemplateContent()
        ]);

    }

    public function down()
    {
        Schema::dropIfExists('viamage_invoicer_financial_statement_templates');
    }

    private function getDefaultTemplateContent()
    {
        return '
<div class="title-page page">
    <h3>{{ company.company }} (AUDIT EXEMPT COMPANY*)</h3>
    <h4>DIRECTORS’ REPORT & FINANCIAL STATEMENTS</h4>
    <h4>YEAR ENDED 31 DECEMBER {{ report.year }}</h4>
    <h5>Registered No. {{ company.regNumber }}</h5>
    <p class="bottom-note">* Viamage Limited is a small company as defined by the Companies Act 2014 and is availing itself of the audit exemption provided for by Chapter 15 of Part 6 of the Companies Act 2014</p>
</div>

<div class="page">
    {{ report_index|raw }}
</div>

<div class="statements">
    {% for directors_statements as title,statement %}
        <div class="statement">
        <h4>{{ title }}</h4>
            {{ statement|raw }}
        </div>
    {% endfor %}
</div>

<div class="statement">
     <h4>Accountants Report</h4>
    {{ accountants_report|raw }}
</div>

<div class="report">
    <h4>Profit and Loss</h4>
    {{ report.profit_and_loss }}
</div>

<div class="report">
    <h4>Balance Sheet</h4>
    {{ report.balance_sheet|raw }}
</div>

<div class="statement">
    <h4>Notes</h4>
    {{ report.notes|raw }}
</div>
        ';
    }
}
