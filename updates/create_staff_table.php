<?php namespace Viamage\Invoicer\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateStaffTable extends Migration
{
    public function up()
    {
        Schema::create('viamage_invoicer_staff', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->mediumText('details');
            $table->string('type');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('viamage_invoicer_staff');
    }
}
