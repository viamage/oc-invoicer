<?php namespace Viamage\Invoicer\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class AddInvoiceKey
 * @package Viamage\Invoicer\Updates
 */
class AddPgColumnsToInvoice extends Migration
{
    public function up()
    {
        Schema::table(
            'viamage_invoicer_invoices',
            function (Blueprint $table) {
                $table->integer('order_id')->after('hash_id')->nullable()->index();
                $table->integer('payment_id')->after('order_id')->nullable()->index();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'viamage_invoicer_invoices',
            function (Blueprint $table) {
                $table->dropColumn('order_id');
                $table->dropColumn('payment_id');
            }
        );
    }
}
