<?php namespace Viamage\Invoicer\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class AddInvoiceKey
 * @package Viamage\Invoicer\Updates
 */
class AddDisplayedToInvoice extends Migration
{
    public function up()
    {
        Schema::table(
            'viamage_invoicer_invoices',
            function (Blueprint $table) {
                $table->boolean('is_displayed')->after('is_paid')->default(true);
            }
        );
    }

    public function down()
    {
        Schema::table(
            'viamage_invoicer_invoices',
            function (Blueprint $table) {
                $table->dropColumn('is_displayed');
            }
        );
    }
}
