<?php namespace Viamage\Website\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class AddDefaultToShortUrl
 * @package Viamage\Website\Updates
 */
class AddUserInvoicePrefix extends Migration
{

    public function up()
    {
        Schema::table(
            'backend_users',
            function (Blueprint $table) {
                $table->string('invoice_prefix')->after('email')->nullable();
            }
        );
    }

    public function down()
    {

    }

}
