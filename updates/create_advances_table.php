<?php
/**
 * Copyright (c) 2018 Viamage Limited
 * All Rights Reserved
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Viamage Limited and its suppliers, if any.
 *  The intellectual and technical concepts contained herein
 *  are proprietary to Viamage Limited and its suppliers and are
 *  protected by trade secret or copyright law, if not specified otherwise.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Viamage Limited.
 *
 */

namespace Viamage\Invoicer\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateAdvancesTable extends Migration
{
    public function up()
    {
        Schema::create(
            'viamage_invoicer_advances',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('label');
                $table->double('amount');
                $table->double('ratio')->default(1.00);
                $table->integer('invoice_id');
                $table->dateTime('payment_date');
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('viamage_invoicer_advances');
    }
}


