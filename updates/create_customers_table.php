<?php namespace Viamage\Invoicer\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateCustomersTable
 * @package Viamage\Invoicer\Updates
 */
class CreateCustomersTable extends Migration
{
    public function up()
    {
        Schema::create('viamage_invoicer_customers', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('company');
            $table->string('slug');
            $table->string('name')->nullable();
            $table->string('address_1');
            $table->string('address_2')->nullable();
            $table->string('zip');
            $table->string('city');
            $table->string('district')->nullable();
            $table->integer('country_id');
            $table->string('vat_number')->nullable();
            $table->string('reg_number')->nullable();
            $table->integer('keeper_id')->index();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('viamage_invoicer_customers');
    }
}
