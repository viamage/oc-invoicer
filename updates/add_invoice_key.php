<?php namespace Viamage\Invoicer\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class AddInvoiceKey
 * @package Viamage\Invoicer\Updates
 */
class AddInvoiceKey extends Migration
{
    public function up()
    {
        Schema::table(
            'viamage_invoicer_invoices',
            function (Blueprint $table) {
                $table->string('hash_id')->after('invoice_number')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'viamage_invoicer_invoices',
            function (Blueprint $table) {
                $table->dropColumn('hash_id');
            }
        );
    }
}
