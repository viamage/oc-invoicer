<?php namespace Viamage\Invoicer\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateCostInvoicesTable
 * @package Viamage\Invoicer\Updates
 */
class CreateCostInvoicesTable extends Migration
{
    public function up()
    {
        Schema::create('viamage_invoicer_cost_invoices', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->mediumText('seller');
            $table->string('invoice_number');
            $table->string('currency');
            $table->double('ratio')->default(1.00);
            $table->double('total_net');
            $table->double('total_tax');
            $table->double('total_gross');
            $table->dateTime('invoice_date');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('viamage_invoicer_cost_invoices');
    }
}
