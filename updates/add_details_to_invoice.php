<?php namespace Viamage\Invoicer\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class AddDetailsToInvoice
 * @package Viamage\Invoicer\Updates
 */
class AddDetailsToInvoice extends Migration
{
    public function up()
    {
        Schema::table(
            'viamage_invoicer_invoices',
            function (Blueprint $table) {
                $table->text('details')->after('payment_method')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'viamage_invoicer_invoices',
            function (Blueprint $table) {
                $table->dropColumn('details');
            }
        );
    }
}
/**
 * Copyright (c) 2018 Viamage Limited
 * All Rights Reserved
 *
 *  NOTICE:  All information contained herein is, and remains
 *  the property of Viamage Limited and its suppliers, if any.
 *  The intellectual and technical concepts contained herein
 *  are proprietary to Viamage Limited and its suppliers and are
 *  protected by trade secret or copyright law, if not specified otherwise.
 *  Dissemination of this information or reproduction of this material
 *  is strictly forbidden unless prior written permission is obtained
 *  from Viamage Limited.
 *
 */


