<?php namespace Viamage\Invoicer\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateInvoicesTable
 * @package Viamage\Invoicer\Updates
 */
class CreateInvoicesTable extends Migration
{
    public function up()
    {
        Schema::create('viamage_invoicer_invoices', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('user_id')->index();
            $table->integer('customer_id')->index();
            $table->string('invoice_number');
            $table->text('goods');
            $table->string('currency');
            $table->double('ratio')->default(1.00);
            $table->double('total_net');
            $table->double('total_gross');
            $table->double('total_net_eur');
            $table->double('total_gross_eur');
            $table->dateTime('sale_date');
            $table->dateTime('issue_date');
            $table->dateTime('due_date');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('viamage_invoicer_invoices');
    }
}
