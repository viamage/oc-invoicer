<?php namespace Viamage\Invoicer\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class AddMailColumns
 * @package Viamage\Invoicer\Updates
 */
class AddMailColumns extends Migration
{
    public function up()
    {
        Schema::table(
            'viamage_invoicer_invoices',
            function (Blueprint $table) {
                $table->boolean('is_sent')->after('is_paid')->default(false);
                $table->integer('reminders_count')->after('is_sent')->default(0);
                $table->dateTime('last_reminder')->nullable();
            }
        );
    }

    public function down()
    {
        Schema::table(
            'viamage_invoicer_invoices',
            function (Blueprint $table) {
                $table->dropColumn('is_sent');
                $table->dropColumn('reminders_count');
            }
        );
    }
}
