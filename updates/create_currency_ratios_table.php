<?php namespace Viamage\Invoicer\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCurrencyRatiosTable extends Migration
{
    public function up()
    {
        Schema::create('viamage_invoicer_currency_ratios', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->date('date');
            $table->text('ratios');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('viamage_invoicer_currency_ratios');
    }
}
