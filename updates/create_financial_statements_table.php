<?php namespace Viamage\Invoicer\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateFinancialStatementsTable extends Migration
{
    public function up()
    {
        Schema::create(
            'viamage_invoicer_financial_statements',
            function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('year');
                $table->string('version')->default('1');
                $table->mediumText('information');
                $table->integer('directors_report_id');
                $table->integer('accountants_report_id');
                $table->longText('profit_and_loss');
                $table->longText('balance_sheet');
                $table->text('notes');
                $table->timestamps();
            }
        );
    }

    public function down()
    {
        Schema::dropIfExists('viamage_invoicer_financial_statements');
    }
}
