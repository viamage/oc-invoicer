<?php namespace Viamage\Invoicer\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateDirectorsReportsTable extends Migration
{
    public function up()
    {
        Schema::create('viamage_invoicer_directors_reports', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('year');
            $table->longText('content');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('viamage_invoicer_directors_reports');
    }
}
