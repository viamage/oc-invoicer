<?php namespace Viamage\Invoicer\Updates;

use Illuminate\Database\Schema\Blueprint;
use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class AddInvoiceKey
 * @package Viamage\Invoicer\Updates
 */
class AddIndexColumn extends Migration
{
    public function up()
    {
        Schema::table(
            'viamage_invoicer_financial_statements',
            function (Blueprint $table) {
                $table->text('index')->after('accountants_report_id');
            }
        );
    }

    public function down()
    {
        Schema::table(
            'viamage_invoicer_financial_statements',
            function (Blueprint $table) {
                $table->dropColumn('index');
            }
        );
    }
}
