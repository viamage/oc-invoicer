<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 10/2/17
 * Time: 1:19 PM
 */

namespace Viamage\Invoicer\Contracts;

/**
 * Interface InvoiceModel
 * @package Viamage\Invoicer\Contracts
 */
interface InvoiceModel
{
    /**
     * @return string
     */
    public function getInvoiceNumber(): string;

    /**
     * @return string
     */
    public function getIssuer(): string;

    /**
     * @return string
     */
    public function getReceiver(): string;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @return string
     */
    public function getIssueDate(): string;

    /**
     * @return string
     */
    public function getEurNet(): string;

    /**
     * @return string
     */
    public function getEurTax(): string;

    /**
     * @return string
     */
    public function getCurrency(): string;

    /**
     * @return float
     */
    public function getRatio(): float;
}