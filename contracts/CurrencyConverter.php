<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 10/5/17
 * Time: 10:27 AM
 */

namespace Viamage\Invoicer\Contracts;

use Carbon\Carbon;
use Keios\MoneyRight\Money;

interface CurrencyConverter
{
    public function convert(Money $amount, $targetIso, Carbon $date = null);

    public function readLatest();

    public function readFor($date);
}