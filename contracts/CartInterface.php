<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/27/17
 * Time: 2:19 PM
 */

namespace Viamage\Invoicer\Contracts;

interface CartInterface
{
    /**
     * @return mixed
     */
    public function getAllProducts();

    /**
     * @return mixed
     */
    public function getCount();

    /**
     * @param array $product
     *
     * @return mixed
     */
    public function addProduct(array $product);

    /**
     * @param array $ids
     *
     * @return mixed
     */
    public function removeProducts(array $ids);
}