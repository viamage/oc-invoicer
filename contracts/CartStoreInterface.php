<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 12/27/17
 * Time: 2:20 PM
 */

namespace Viamage\Invoicer\Contracts;

use Keios\PaymentGateway\ValueObjects\Cart;

interface CartStoreInterface
{
    /**
     * @param Cart $cart
     *
     * @return mixed
     */
    public function save(Cart $cart);

    /**
     * @return mixed
     */
    public function load();

    /**
     * @return mixed
     */
    public function delete();
}