<?php namespace Viamage\Invoicer\Models;

use Model;

/**
 * FinancialStatementTemplate Model
 */
class FinancialStatementTemplate extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_invoicer_financial_statement_templates';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}
