<?php namespace Viamage\Invoicer\Models;

use Backend\Models\UserGroup;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Keios\MoneyRight\Currency;
use October\Rain\Database\Model;

/**
 * Settings Model
 */
class Settings extends Model
{

    /**
     * @var array
     */
    public $implement = ['System.Behaviors.SettingsModel'];

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * A unique code of settings
     */
    public $settingsCode = 'viamage_invoicer_settings';

    /**
     * Reference to field configuration
     */
    public $settingsFields = 'fields.yaml';

    /**
     * Initialization of the defaults
     */
    public function initSettingsData()
    {
    }

    public function getInvoiceCountryOptions()
    {
        return [
            'IE' => 'Ireland',
        ];
    }
}
