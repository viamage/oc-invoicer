<?php namespace Viamage\Invoicer\Models;

use Model;

/**
 * Staff Model
 */
class Staff extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_invoicer_staff';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @return array
     */
    public function getTypeOptions()
    {
        return [
            'director'   => 'Director',
            'secretary'  => 'Secretary',
            'accountant' => 'Accountant',
            'solicitor'  => 'Solicitor',
            'banker'     => 'Banker',
        ];
    }

    /**
     * @param $attribute
     * @return mixed
     */
    public function getTypeAttribute($attribute)
    {
        if ($attribute) {
            return $this->getTypeOptions()[$attribute];
        }

        return $attribute;
    }
}
