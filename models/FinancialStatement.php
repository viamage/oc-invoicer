<?php namespace Viamage\Invoicer\Models;

use Model;

/**
 * FinancialStatement Model
 */
class FinancialStatement extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_invoicer_financial_statements';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $attachOne = [
        'pdf' => \File::class,
    ];
}
