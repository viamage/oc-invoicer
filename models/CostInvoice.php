<?php namespace Viamage\Invoicer\Models;

use Backend\Models\User;
use Carbon\Carbon;
use Keios\MoneyRight\Money;
use Keios\MoneySupport\Classes\CurrencyList;
use Model;
use October\Rain\Exception\ApplicationException;
use System\Models\File;
use Viamage\Invoicer\Classes\IrishCurrencyConverter;
use Viamage\Invoicer\Contracts\InvoiceModel;

/**
 * CostInvoice Model
 */
class CostInvoice extends Model implements InvoiceModel
{

    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_invoicer_cost_invoices';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    protected $rules = [
        'seller'         => 'required',
        'invoice_number' => 'required',
        'currency'       => 'required',
        'invoice_date'   => 'required',
    ];

    /**
     * @var array
     */
    public $belongsTo = [
        'user' => User::class,
    ];

    /**
     * @var array
     */
    public $attachOne = [
        'doc' => [File::class],
    ];


    /**
     * @throws ApplicationException
     * @throws \October\Rain\Exception\ApplicationException
     * @throws \PHPExcel_Reader_Exception
     */
    public function beforeCreate()
    {
        if (!$this->ratio) {
            $converter = new IrishCurrencyConverter();
            $ratios = $converter->readFor($this->invoice_date);
            $ratios['EUR'] = 1.00;
            if (!array_key_exists($this->currency, $ratios)) {
                throw new ApplicationException('Currency '.$this->currency.' is not supported');
            }
            $this->ratio = $ratios[$this->currency];
        }
    }

    /**
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function beforeUpdate()
    {
        $currency = $this->currency;
        /** @var Money $net */
        $net = Money::$currency($this->total_net);
        $tax = Money::$currency($this->total_tax);
        $gross = $net->add($tax);

        $this->total_gross = $gross->getAmountString();
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'incoming';
    }

    /**
     * @return string
     */
    public function getIssueDate(): string
    {
        $date = new Carbon($this->invoice_date);

        return $date->format('d-m-Y');
    }

    /**
     * @return string
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function getEurNet(): string
    {
        /** @var Money $raw */
        $raw = Money::EUR($this->total_net);
        $raw = $raw->divide($this->ratio);

        return $raw->getAmountString();
    }

    /**
     * @return string
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function getEurTax(): string
    {
        /** @var Money $raw */
        $raw = Money::EUR($this->total_tax);
        $raw = $raw->divide($this->ratio);

        return $raw->getAmountString();
    }

    /**
     * @return string
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function getEurGross(): string
    {
        /** @var Money $raw */
        $raw = Money::EUR($this->total_gross);
        $raw = $raw->divide($this->ratio);

        return $raw->getAmountString();
    }

    /**
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function getCurrencyOptions(): array
    {
        $currencies = CurrencyList::all();
        $converter = new IrishCurrencyConverter();
        $ratios = $converter->readLatest();
        $result['EUR'] = 'Euro';
        foreach ($ratios as $iso => $ratio) {
            $result[$iso] = $currencies[$iso];
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getPaymentMethodOptions(): array
    {
        return [
            'Bank Transfer'  => 'Bank Transfer',
            'Online Payment' => 'Online Payment',
            'Credit Card'    => 'Credit Card',
            'Cash'           => 'Cash',
            'Barter'         => 'Barter',
        ];
    }

    /**
     * @return string
     */
    public function getInvoiceNumber(): string
    {
        return $this->invoice_number;
    }

    /**
     * @return string
     */
    public function getIssuer(): string
    {
        return $this->seller;
    }

    /**
     * @return string
     */
    public function getReceiver(): string
    {
        return $this->user->email;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return float
     */
    public function getRatio(): float
    {
        return (float)$this->ratio;
    }
}
