<?php namespace Viamage\Invoicer\Models;

use Model;

/**
 * CurrencyRatio Model
 */
class CurrencyRatio extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_invoicer_currency_ratios';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
