<?php namespace Viamage\Invoicer\Models;

use Model;

/**
 * DirectorsReport Model
 */
class DirectorsReport extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_invoicer_directors_reports';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @return string
     */
    public function getContent(){
        return $this->content;
    }
}
