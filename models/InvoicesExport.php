<?php
/**
 * Created by PhpStorm.
 * User: jin
 * Date: 10/3/17
 * Time: 9:02 AM
 */

namespace Viamage\Invoicer\Models;

use Carbon\Carbon;
use Keios\MoneyRight\Money;
use Viamage\Invoicer\Contracts\InvoiceModel;

/**
 * Class InvoicesExport
 * @package Viamage\Invoicer\Models
 */
class InvoicesExport extends \Backend\Models\ExportModel
{
    /**
     * @var static
     */
    private $fiscalYearStart;
    /**
     * @var Carbon
     */
    private $fiscalYearEnd;

    /**
     * InvoicesExport constructor.
     */
    public function __construct()
    {
        $now = Carbon::now();
        $foundation = new Carbon('21-09-2017');
        $ardDay = $foundation->addMonths(6)->format('d-M');
        $ard = new Carbon($ardDay.'-'.date('Y'));
        if ($now->diffInDays($ard, false) < 0) {
            $ard->addYear();
        }
        $this->fiscalYearEnd = $ard;
        $this->fiscalYearStart = $ard->copy()->subYear();
    }

    /**
     * @param      $columns
     * @param null $sessionKey
     * @return mixed
     */
    public function exportData($columns, $sessionKey = null)
    {
        $outgoing = $this->getCurrentYearOutgoing();
        $incoming = $this->getCurrentYearIncoming();

        /** @var CostInvoice $incomingInvoice */
        foreach ($incoming as $incomingInvoice) {
            $key = $incomingInvoice->getIssueDate().$incomingInvoice->created_at->format('dmYhms');
            $invoices[$key] = $this->getTemplate($incomingInvoice);
        }
        /** @var Invoice $outgoingInvoice */
        foreach ($outgoing as $outgoingInvoice) {
            $key = $outgoingInvoice->getIssueDate().$outgoingInvoice->created_at->format('dmYhms');
            $invoices[$key] = $this->getTemplate($outgoingInvoice);
        }
        ksort($invoices);

        return $invoices;
    }

    /**
     * @param InvoiceModel $invoice
     * @return array
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    private function getTemplate(InvoiceModel $invoice)
    {
        /** @var Money $netEur */
        /** @var Money $taxEur */
        /** @var Money $grossEur */
        $ratio = $invoice->getRatio();
        $netEur = Money::EUR($invoice->getEurNet());
        $taxEur = Money::EUR($invoice->getEurTax());
        $grossEur = $netEur->add($taxEur);
        $nativeNet = $netEur->multiply($ratio);
        $nativeTax = $taxEur->multiply($ratio);
        $nativeGross = $grossEur->multiply($ratio);

        return [
            'date'               => $invoice->getIssueDate(),
            'number'             => $invoice->getInvoiceNumber(),
            'issuer'             => $invoice->getIssuer(),
            'receiver'           => $invoice->getReceiver(),
            'type'               => $invoice->getType(),
            'currency'           => $invoice->getCurrency(),
            'ratio'              => $invoice->getRatio(),
            'net_native_value'   => $nativeNet->getAmountString(),
            'tax_native_value'   => $nativeTax->getAmountString(),
            'gross_native_value' => $nativeGross->getAmountString(),
            'net_eur_value'      => $invoice->getEurNet(),
            'tax_eur_value'      => $invoice->getEurTax(),
            'gross_eur_value'    => $grossEur->getAmountString(),
        ];
    }

    /**
     * @return mixed
     */
    private function getCurrentYearOutgoing()
    {
        return Invoice::where('issue_date', '>=', $this->fiscalYearStart)->where(
            'issue_date',
            '<=',
            $this->fiscalYearEnd
        )->get();
    }

    /**
     * @return mixed
     */
    private function getCurrentYearIncoming()
    {
        return CostInvoice::where('invoice_date', '>=', $this->fiscalYearStart)->where(
            'invoice_date',
            '<=',
            $this->fiscalYearEnd
        )->get();
    }

}