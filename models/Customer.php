<?php namespace Viamage\Invoicer\Models;

use Backend\Models\User;
use Keios\MoneySupport\Classes\CurrencyList;
use Model;
use RainLab\Location\Models\Country;
use Viamage\Invoicer\Classes\IrishCurrencyConverter;

/**
 * Customer Model
 */
class Customer extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_invoicer_customers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $rules = [
        'company' => 'required',
    ];

    /**
     * @var array
     */
    public $hasMany = [
        'invoices' => Invoice::class,
    ];

    /**
     * @var array
     */
    public $belongsTo = [
        'keeper'  => User::class,
        'country' => Country::class,
    ];

    /**
     * @return array
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function getCurrencyOptions(): array
    {
        $currencies = CurrencyList::all();
        $converter = new IrishCurrencyConverter();
        $ratios = $converter->readLatest();
        $result['EUR'] = 'Euro';
        foreach ($ratios as $iso => $ratio) {
            $result[$iso] = $currencies[$iso];
        }

        return $result;
    }
}
