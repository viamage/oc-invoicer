<?php namespace Viamage\Invoicer\Models;

use Backend\Models\User;
use Carbon\Carbon;
use Hashids\Hashids;
use Keios\MoneyRight\Money;
use Keios\MoneySupport\Classes\CurrencyList;
use Model;
use October\Rain\Exception\ApplicationException;
use Viamage\Invoicer\Classes\InvoiceCalculator;
use Viamage\Invoicer\Classes\InvoiceLinkGenerator;
use Viamage\Invoicer\Classes\IrishCurrencyConverter;
use Viamage\Invoicer\Contracts\CurrencyConverter;
use Viamage\Invoicer\Contracts\InvoiceModel;
use Viamage\Invoicer\ValueObjects\Company;
use Viamage\Invoicer\ValueObjects\Good;
use System\Models\File;

/**
 * Invoice Model
 */
class Invoice extends Model implements InvoiceModel
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_invoicer_invoices';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    protected $jsonable = ['goods'];

    /**
     * @var array
     */
    protected $rules = [
        'sale_date'      => 'required',
        'issue_date'     => 'required',
        'due_date'       => 'required',
        'invoice_number' => 'required',
        'goods'          => 'required',
    ];
    /**
     * @var array
     */
    protected $customMessages = [
        'goods.required' => 'Please add at least one product.',
    ];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'user'     => User::class,
        'customer' => Customer::class,
    ];

    public $hasMany = [
        'advances' => Advance::class
    ];

    /**
     * @var array
     */
    public $attachOne = [
        'pdf' => [File::class],
    ];

    public function getInvoiceNumberAttribute($value)
    {
        if ($value) {
            return $value;
        }

        return $this->generateInvoiceNumberSuggestion();
    }

    private function generateInvoiceNumberSuggestion()
    {
        $settings = Settings::instance();
        $user = \BackendAuth::getUser();
        $now = Carbon::now();
        $rules = $settings->get('invoice_number_rules');
        if (!$rules || !$user) {
            return '';
        }
        $login = $user->login;
        if(isset($user->invoice_prefix)){
            $login = $user->invoice_prefix;
        }

        $month = $now->month;
        if (\strlen($now->month) === 1) {
            $month = '0'.$now->month;
        }
        $shortYear = substr($now->year, 2);
        $userBased = $settings->get('invoice_user_count', false);
        $number = str_replace(['$L', '$MM', '$YY'], [$login, $month, $now->year], $rules);
        if (str_contains($number, '$YC')) {
            $fullYearlyCount = $yearlyCount = $this->getYearlyInvoiceCount($now, $user, $userBased) + 1;
            if (\strlen($yearlyCount) === 1) {
                $fullYearlyCount = '0'.$yearlyCount;
            }
            $number = str_replace(['$YCC', '$YC'], [$fullYearlyCount, $yearlyCount], $number);
        }

        if (str_contains($number, '$C')) {
            $fullMonthlyCount = $monthlyCount = $this->getMonthlyInvoiceCount($now, $user, $userBased) + 1;
            if (\strlen($monthlyCount) === 1) {
                $fullMonthlyCount = '0'.$monthlyCount;
            }
            $number = str_replace(['$CC', '$C'], [$fullMonthlyCount, $monthlyCount], $number);
        }

        $number = str_replace(['$M', '$Y'], [$now->month, $shortYear], $number);

        return $number;
    }

    private function getMonthlyInvoiceCount(Carbon $now, User $user, bool $userBased)
    {
        $q = self::where('issue_date', '>', $now->startOfMonth());
        if ($userBased) {
            $q = $q->where('user_id', $user->id);
        }

        return $q->count();
    }

    private function getYearlyInvoiceCount(Carbon $now, User $user, bool $userBased)
    {
        $q = self::where('issue_date', '>', $now->startOfYear());
        if ($userBased) {
            $q = $q->where('user_id', $user->id);
        }

        return $q->count();
    }

    /**
     * @throws \ValidationException
     * @throws ApplicationException
     * @throws \PHPExcel_Reader_Exception
     * @throws \October\Rain\Exception\ApplicationException
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     * @throws \PHPExcel_Exception
     */
    public function beforeCreate()
    {
        $issueDate = $this->getIssueDate();
        $carbon = new Carbon($issueDate);
        $carbon->subDay(1);
        $offlineRatio = CurrencyRatio::where('date', $carbon)->first();
        if($offlineRatio && json_decode($offlineRatio->ratios, true)){
            $ratios = json_decode($offlineRatio->ratios, true);
        } else {
            /** @var CurrencyConverter $converter */
            $converter = \App::make(CurrencyConverter::class);
            $ratios = $converter->readFor($issueDate);
        }

        $ratios['EUR'] = 1.00;
        if (!array_key_exists($this->currency, $ratios)) {
            throw new ApplicationException('Currency '.$this->currency.' is not supported');
        }
        $this->ratio = $ratios[$this->currency];
        $this->calculateCosts();
        if(!$this->getDetails()){
            $this->details = '';
        }
    }

    /**
     * @throws \Hashids\HashidsException
     */
    public function afterCreate()
    {
        $hashids = new Hashids(config('app.key'), 12);
        $hashId = $hashids->encode($this->id);
        $this->hash_id = $hashId;
        $this->save();
    }

    /**
     * @throws \Hashids\HashidsException
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     * @throws \ValidationException
     */
    public function beforeUpdate()
    {
        $hashids = new Hashids(config('app.key'), 12);
        $hashId = $hashids->encode($this->id);
        $this->hash_id = $hashId;
        $this->calculateCosts();
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return 'outgoing';
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        $company = new Company();
        $company->fromModel($this->customer);

        return $company;
    }

    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @return string
     */
    public function getInvoiceNumber(): string
    {
        return $this->invoice_number;
    }

    /**
     * @return false|string
     */
    public function getInvoiceNumberSlug()
    {
        $number = $this->invoice_number;

        return mb_ereg_replace("([^\w\s\d\-_~,;\[\]\(\).])", '', $number);

    }

    /**
     * @return string
     */
    public function getSaleDate(): string
    {
        $date = new Carbon($this->sale_date);

        return $date->format('d-m-Y');
    }

    /**
     * @return string
     */
    public function getDueDate(): string
    {
        $date = new Carbon($this->due_date);

        return $date->format('d-m-Y');
    }

    /**
     * @return string
     */
    public function getIssueDate(): string
    {
        $date = new Carbon($this->issue_date);

        return $date->format('d-m-Y');
    }

    /**
     * @return mixed
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function getPaymentMethod()
    {
        return $this->payment_method;
    }

    /**
     * @return string
     * @throws \ApplicationException
     * @throws \Exception
     */
    public function getInvoiceLink(): string
    {
        $generator = new InvoiceLinkGenerator();

        return $generator->getInvoiceLink($this);
    }

    public function getInvoiceInternalLink(): string
    {
        $generator = new InvoiceLinkGenerator();

        return $generator->getInvoiceInternalLink($this);
    }

    /**
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     * @throws \ValidationException
     */
    private function calculateCosts()
    {
        $goods = [];

        foreach ($this->goods as $good) {
            $goodVo = new Good();
            $goodVo->fromModelEntry($good, $this->ratio, $this->currency);
            $goods[] = $goodVo;
        }
        $totals = InvoiceCalculator::countTotals($goods, $this->currency);
        $this->total_net = $totals->totalNet->getAmountString();
        $this->total_gross = $totals->totalGross->getAmountString();
        $this->total_net_eur = $totals->totalEurNet->getAmountString();
        $this->total_gross_eur = $totals->totalEurGross->getAmountString();
    }

    /**
     * @return string
     */
    public function getEurNet(): string
    {
        /** @var Money $raw */
        $raw = Money::EUR($this->total_net_eur);

        return $raw->getAmountString();
    }

    /**
     * @return string
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function getEurTax(): string
    {
        /** @var Money $net */
        /** @var Money $gross */
        $net = Money::EUR($this->total_net_eur);
        $gross = Money::EUR($this->total_gross_eur);
        /** @var Money $raw */
        $raw = $gross->subtract($net);

        return $raw->getAmountString();
    }

    /**
     * @return Money
     */
    public function getTotalNet(): \Keios\MoneyRight\Money
    {
        $currency = $this->getCurrency();
        if (!$totalNet = $this->total_net) {
            $totalNet = 0;
        }

        return Money::$currency($totalNet);
    }

    /**
     * @return Money
     */
    public function getTotalGross()
    {
        $currency = $this->getCurrency();
        if (!$totalGross = $this->total_gross) {
            $totalGross = 0;
        }

        return Money::$currency($totalGross);
    }

    /**
     * @return Money
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     */
    public function getTotalTax(): \Keios\MoneyRight\Money
    {
        return $this->getTotalGross()->subtract($this->getTotalNet());
    }

    /**
     * @return string
     */
    public function getEurGross(): string
    {
        /** @var Money $raw */
        $raw = Money::EUR($this->total_gross_eur);

        return $raw->getAmountString();
    }

    /**
     * @return mixed
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function getCurrencyOptions(): array
    {
        $currencies = CurrencyList::all();
        $converter = new IrishCurrencyConverter();
        $ratios = $converter->readLatest();
        $result['EUR'] = 'Euro';
        foreach ($ratios as $iso => $ratio) {
            $result[$iso] = $currencies[$iso];
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getPaymentMethodOptions(): array
    {
        return [
            'Bank Transfer'  => 'Bank Transfer',
            'Online Payment' => 'Online Payment',
            'Credit Card'    => 'Credit Card',
            'Cash'           => 'Cash',
            'Barter'         => 'Barter',
        ];
    }

    /**
     * @return boolean
     */
    public function isPaid(): bool
    {
        return (bool)$this->is_paid;
    }

    /**
     * @return boolean
     */
    public function isDisplayed(): bool
    {
        return (bool)$this->is_displayed;
    }

    /**
     * @return string
     */
    public function getIssuer(): string
    {
        return $this->user->email;
    }

    /**
     * @return string
     */
    public function getReceiver(): string
    {
        return $this->getCompany()->company;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @return float
     */
    public function getRatio(): float
    {
        return (float)$this->ratio;
    }
}
