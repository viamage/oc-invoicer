<?php namespace Viamage\Invoicer\Models;

use Model;

/**
 * DirectorsStatement Model
 */
class DirectorsStatement extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_invoicer_directors_statements';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @return string
     */
    public function getContent(){
        return $this->content;
    }
}
