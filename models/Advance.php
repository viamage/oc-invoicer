<?php namespace Viamage\Invoicer\Models;

use Backend\Models\User;
use Carbon\Carbon;
use Hashids\Hashids;
use Keios\MoneyRight\Money;
use Keios\MoneySupport\Classes\CurrencyList;
use Model;
use October\Rain\Exception\ApplicationException;
use Viamage\Invoicer\Classes\InvoiceCalculator;
use Viamage\Invoicer\Classes\InvoiceLinkGenerator;
use Viamage\Invoicer\Classes\IrishCurrencyConverter;
use Viamage\Invoicer\Contracts\CurrencyConverter;
use Viamage\Invoicer\Contracts\InvoiceModel;
use Viamage\Invoicer\ValueObjects\Company;
use Viamage\Invoicer\ValueObjects\Good;
use System\Models\File;

/**
 * Invoice Model
 */
class Advance extends Model
{
    use \October\Rain\Database\Traits\Validation;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'viamage_invoicer_advances';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array
     */
    protected $jsonable = [];

    /**
     * @var array
     */
    protected $rules = [
        'label' => 'required',
        'invoice_id' => 'required',
        'amount'     => 'required',
    ];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'invoice' => Invoice::class,
    ];

    /**
     * @var array
     */
    public $attachOne = [
    ];

    /**
     * @throws \ValidationException
     * @throws ApplicationException
     * @throws \PHPExcel_Reader_Exception
     * @throws \October\Rain\Exception\ApplicationException
     * @throws \Keios\MoneyRight\Exceptions\InvalidArgumentException
     * @throws \PHPExcel_Exception
     */
    public function beforeCreate()
    {
        $issueDate = $this->getIssueDate();
        $carbon = new Carbon($issueDate);
        $carbon->subDay(1);
        $offlineRatio = CurrencyRatio::where('date', $carbon)->first();
        if ($offlineRatio) {
            $ratios = json_decode($offlineRatio->ratios, true);
        } else {
            /** @var CurrencyConverter $converter */
            $converter = \App::make(CurrencyConverter::class);
            $ratios = $converter->readFor($issueDate);
        }

        $ratios['EUR'] = 1.00;
        if (!array_key_exists($this->invoice->currency, $ratios)) {
            throw new ApplicationException('Currency '.$this->currency.' is not supported');
        }
        $this->ratio = $ratios[$this->invoice->currency];
    }

    /**
     * @return string
     */
    public function getIssueDate(): string
    {
        $date = new Carbon($this->invoice->issue_date);

        return $date->format('d-m-Y');
    }

    /**
     * @return mixed
     */
    public function getCustomer(): Customer
    {
        return $this->invoice->customer;
    }
}
